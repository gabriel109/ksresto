<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $procedure = "
        // CREATE PROCEDURE `create_bills`()
        // BEGIN
        // DECLARE v_finished INTEGER DEFAULT 0;
        // DECLARE v_user_id integer;
        // DECLARE v_service_id integer;
        // DECLARE v_service_aux integer;
        // DECLARE v_amount DECIMAL(8,2);
        // DECLARE v_bills_expired integer;
        // DECLARE Customer_Cursor CURSOR FOR 
        //     SELECT id, service_id, (SELECT COUNT(*) FROM bills b WHERE b.date_paid IS NULL AND b.date_expiration < DATE(NOW()) AND u.id = b.user_id)
        //     FROM users u WHERE DAY(u.service_started) = DAY(NOW()) AND u.service_started != DATE(NOW()) AND u.service_id IS NOT NULL;
        // DECLARE CONTINUE HANDLER 
        // FOR NOT FOUND SET v_finished = 1;
        // OPEN Customer_Cursor;
        
        // create_bills: LOOP
        //     FETCH Customer_Cursor INTO v_user_id, v_service_id, v_bills_expired;
        //     IF v_finished = 1 THEN 
        //         LEAVE create_bills;
        //     END IF;
            
        //     SET v_service_aux = v_service_id;
        //     IF v_bills_expired > 0 THEN
        //         SET v_service_aux = 1;
        //         IF v_bills_expired >= 3 THEN
        //             DELETE FROM bills WHERE date_paid IS NULL AND user_id = v_user_id;
        //         END IF;
        //     END IF;
            
		// 	UPDATE users SET orders_emitted = 0,  service_changed = false,
		// 			orders_limit = (SELECT orders_limit FROM services WHERE id = v_service_aux) 
        //             WHERE id = v_user_id;
            
        //     IF v_service_id > 1 THEN
        //     BEGIN
        //         SET v_amount = (SELECT price FROM services WHERE id = v_service_id);
        //         INSERT INTO bills (user_id, service_id, amount, date_expiration, created_at, updated_at) 
        //             VALUES (v_user_id, v_service_id, v_amount , DATE_ADD(DATE(NOW()), INTERVAL 21 DAY), NOW(), NOW());
        //     END;
        //     END IF;

        // END LOOP create_bills;
        // END
        // ";
        
        // DB::unprepared("DROP PROCEDURE IF EXISTS create_bills");
        // DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::unprepared("DROP PROCEDURE IF EXISTS create_bills");
    }
}
