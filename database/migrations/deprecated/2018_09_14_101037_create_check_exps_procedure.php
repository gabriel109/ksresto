<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckExpsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $procedure = "
        // CREATE PROCEDURE `check_expirations` ()
        // BEGIN
        //     DECLARE v_finished INTEGER DEFAULT 0;
        //     DECLARE v_user_id INTEGER;
        //     DECLARE bills_expired_cursor CURSOR FOR
        //         SELECT DISTINCT user_id FROM bills WHERE `status` IS NULL OR `status` = 'rejected' OR `status` = 'cancelled'
        //             AND date_expiration < DATE(NOW());
        //     DECLARE CONTINUE HANDLER
        //     FOR NOT FOUND SET v_finished = 1;

        //     OPEN bills_expired_cursor;
        //     bills_expired: LOOP
        //         FETCH bills_expired_cursor INTO v_user_id;
        //         IF v_finished = 1 THEN
        //             LEAVE bills_expired;
        //         END IF;
        //         UPDATE users SET orders_limit = (SELECT orders_limit FROM services WHERE id=1) WHERE id = v_user_id;
        //     END LOOP bills_expired;
        // END
        // ";
        
        // DB::unprepared("DROP PROCEDURE IF EXISTS check_expirations");
        // DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::unprepared("DROP PROCEDURE IF EXISTS check_expirations");
    }
}
