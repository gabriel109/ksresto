<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::unprepared("CREATE EVENT event_create_bills
        //     ON SCHEDULE
        //     EVERY 1 DAY
        //     STARTS '2018-09-16 00:00:00' ON COMPLETION PRESERVE ENABLE 
        //     DO
        //     CALL create_bills;
        // ");

        // DB::unprepared("CREATE EVENT event_check_expirations
        //     ON SCHEDULE
        //     EVERY 1 DAY
        //     STARTS '2018-09-16 00:00:00' ON COMPLETION PRESERVE ENABLE 
        //     DO
        //     CALL check_expirations;
        // ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::unprepared("DROP EVENT IF EXISTS event_create_bills");
        // DB::unprepared("DROP EVENT IF EXISTS event_check_expirations");
    }
}
