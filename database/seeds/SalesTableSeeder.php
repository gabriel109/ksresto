<?php

use Illuminate\Database\Seeder;

class SalesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sales')->insert([
            'id' => 1,
            'restaurant_id' => 1,
            'table_id' => 1,
            'user_id' => 1,
            'amount' => mt_rand(1, 1000),
        ]);
    }
}