<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'id' => 1,
            'name' => 'Basic',
            'description' => 'Acá va una descripcion.',
            'image' => '/images/enlapanza_oso.jpg',
            'orders_limit' => 150,
            'price' => 0
        ]);
        DB::table('services')->insert([
            'id' => 2,
            'name' => 'Premium',
            'description' => 'Acá va una descripcion.',
            'image' => '/images/papa_oso.jpg',
            'orders_limit' => 1500,
            'price' => 800
        ]);
        DB::table('services')->insert([
            'id' => 3,
            'name' => 'Pro',
            'description' => 'Acá va una descripcion.',
            'image' => '/images/mama_osa.jpg',
            'orders_limit' => 0,
            'price' => 2000
        ]);
    }
}
