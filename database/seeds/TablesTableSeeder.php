<?php

use Illuminate\Database\Seeder;

class TablesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tables')->insert([
            'id' => 1,
            'name' => 'Mesa 1',
            'restaurant_id' => 1,
        ]);
        
        DB::table('tables')->insert([
            'id' => 2,
            'name' => 'Mesa 2',
            'restaurant_id' => 1,
        ]);
    }
}
