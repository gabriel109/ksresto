<?php

use Illuminate\Database\Seeder;

class PlatesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plates')->insert([
            'id' => 1,
            'name' => 'Pizza',
            'price' => 50.58,
            'cost' => 100,
            'restaurant_id' => 1,
        ]);   
        DB::table('plates')->insert([
            'id' => 2,
            'name' => 'Lomito',
            'price' => 70.10,
            'cost' => 150.00,
            'restaurant_id' => 1,
        ]);
    }
}