<?php

use Illuminate\Database\Seeder;
use App\Order;

class OrdersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $order1 = Order::create([
            'id' => 1,
            'sale_id' => 1,
        ]);

        $order1->plates()->attach(1, [
            'price' => 100,
            'quantity' => 1,
        ]);

        $order1->plates()->attach(2, [
            'price' => 150,
            'quantity' => 1,
        ]);
    }
}
