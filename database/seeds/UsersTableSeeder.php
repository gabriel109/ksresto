<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->insert([
            'id' => 1,
            'name' => 'Gabriel',
            'email' => 'gabrieldiez@hotmail.com',
            'password' => bcrypt('123'),
            'restaurant_id' => 1,
        ]);
    }
}
