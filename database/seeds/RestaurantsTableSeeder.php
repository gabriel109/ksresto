<?php

use Illuminate\Database\Seeder;

class RestaurantsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            'id' => 1,
            'name' => 'Gabrielito',
            'service_id' => 1,
        ]);
    }
}
