<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Carbon\Carbon;
use DB;

class UserController extends Controller
{
    public function register(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        
        $user = new User([
            'name' => $request->input('name'),
            'email'=> $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ]);
        $user->service_started = Carbon::now();
        $user->save();
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $credentials = $request->only('email', 'password');
        
        if ( ! $token = JWTAuth::attempt($credentials)) {
            return response([
                'status' => '401',
                'error' => 'invalid.credentials',
                'msg' => 'Email y/o contraseña incorrectos.'
            ], 401);
        }
        return response()->json(compact('token'));
    }

    public function user() {
        $user = User::where('id', (Auth::user()->id))->first();
        return $user;
    }

    public function permissions() {
        $permisions = DB::table('restaurant_user')->where('user_id', Auth::user()->id)->get();
        return $permisions;
    }
	
    public function logout()
    {
        JWTAuth::invalidate();
        return response([
                'status' => 'success',
                'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function refresh()
    {
        return response([
         'status' => 'success'
        ]);
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->fill($request->all());
        $user->save();
    }
}
