<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Table;
use App\Restaurant;

class TableController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('restaurant_id')) {
            $restaurant = Restaurant::find($request->restaurant_id);
            $this->authorize('index', [Table::class, $restaurant]);
            return Table::where('restaurant_id', $request->restaurant_id)->get();
        }
    }

    public function store(Request $request)
    {
        if ($request->has('restaurant_id')) {
            $restaurant = Restaurant::find($request->restaurant_id);
            $this->authorize('store', [Table::class, $restaurant]);
            $table = new Table();
            $table->name = $request->name;
            $table->restaurant_id = $request->restaurant_id;
            $table->save();
            return $table;
        }
    }

    public function show($id)
    {
        $table = Table::find($id);
        $this->authorize('show', $table);
        return $table;
    }

    public function update(Request $request, $id)
    {
        $table = Table::find($id);
        $this->authorize('update', $table);
        $table->fill($request->all());
        $table->save();
    }

    public function destroy(Table $table)
    {
        $this->authorize('destroy', $table);
        $table->delete();
    }
}
