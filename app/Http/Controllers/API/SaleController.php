<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Sale;
use App\Table;
use App\Restaurant;
use App\Order;
use App\Jobs\NewOrder;
use Carbon\Carbon;


class SaleController extends Controller
{

    public function index(Request $request)
    {
        $restaurantId = $request->restaurant_id;
        $restaurant = Restaurant::find($restaurantId);
        $this->authorize('index', [Sale::class, $restaurant]);

        $sales = Sale::with('orders')
            ->when($restaurantId, function ($query) use ($restaurantId) {
                return $query->where('restaurant_id', $restaurantId);
            })
            ->when($request->has('delivery'), function ($query) {
                return $query->where('table_id', null);
            })
            ->when($request->has('plates'), function ($query) {
                return $query->with('orders.plates');
            })
            ->when($request->has('unpaid'), function ($query) {
                return $query->where('paid', false);
            })
            ->orderBy('created_at', 'DESC')
            ->get();
        return $sales;
    }

    public function store(Request $request)
    {
        if ($request->has('restaurant_id')) {
            $restaurant = Restaurant::find($request->restaurant_id);
            $this->authorize('store', [Sale::class, $restaurant]);
            $sale = new Sale($request->all());
            $sale->user_id = auth()->id(); // waiter
            $sale->save();
            if ($request->has('orders')) {
                foreach ($request->orders as $order) {
                    $new_order = $sale->orders()->save(new Order($order));
                    $platesArray = array();
                    foreach ($order['plates'] as $plate) {
                        $platesArray[$plate['id']] = array();
                        $platesArray[$plate['id']]['quantity'] = $plate['pivot']['quantity'];
                    }
                    $new_order->plates()->attach($platesArray);
                    NewOrder::dispatch($new_order);
                }
            }
            return $sale;
        }
    }

    public function show($id, Request $request)
    {
        $sale = Sale::find($id);
        $this->authorize('show', $sale);
        if ($request->has('plates')) {
            return Sale::with('orders.plates')->where('id', $id)->get();
        } else {
            return $sale;
        }
    }

    public function update($id, Request $request)
    {
        $sale = Sale::find($id);
        $this->authorize('update', $sale);
        $sale->fill($request->all());
        $sale->save();
    }

    public function destroy(Sale $sale)
    {
        $this->authorize('destroy', $sale);
        $sale->delete();
    }
}
