<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plate;
use App\Restaurant;

class PlateController extends Controller
{

    public function index(Request $request)
    {
        if ($request->has('restaurant_id')) {
            $restaurant = Restaurant::find($request->restaurant_id);
            $this->authorize('index', [Plate::class, $restaurant]);
            $numPlates = $request->numPlates;
            $plates = Plate::where('restaurant_id', $restaurant->id)
            ->when($numPlates, function ($q) use ($numPlates) {
                return $q->paginate($numPlates);
            }, function ($q){
                return $q->get();
            });
            return $plates;
        }
    }

    public function store(Request $request)
    {
        if ($request->has('restaurant_id')) {
            $restaurant = Restaurant::find($request->restaurant_id);
            $this->authorize('store', [Plate::class, $restaurant]);
            $plate = new Plate($request->all());        
            $plate->save();
            return $plate;
        }
    }

    public function show($id)
    {
        $plate = Plate::find($id);
        $this->authorize('show', $plate);
        return $plate;   
    }

    public function update(Request $request, $id)
    {
        $plate = Plate::find($id);
        $this->authorize('update', $plate);
        $plate->fill($request->all());
        $plate->save();
    }

    public function destroy(Plate $plate)
    {
        $this->authorize('destroy', $plate);
        $plate->delete();
    }
}
