<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;


class ServiceController extends Controller
{

    public function index()
    {
        return Service::all();
    }

    public function show($id)
    {
        return Service::find($id);
    }
}
