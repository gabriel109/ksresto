<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Restaurant;
use App\Sale;
use App\Order;
use App\Events\OrderStoredEvent;
use App\Events\OrderReadyEvent;
use App\Jobs\NewOrder;
use App\Jobs\OrderReady;
use Illuminate\Support\Facades\Queue;

class OrderController extends Controller
{

    public function index(Request $request)
    {
        if ($request->has('restaurant_id')) {
            $restaurant = Restaurant::find($request->restaurant_id);
            $restaurantId = $restaurant->id;
            $ready = $request->ready;
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $this->authorize('indexByRestaurant', [Order::class, $restaurant]);
            // Create query
            $sales = Order::with('sale')
            ->whereHas('sale', function ($query) use ($restaurantId) {
                $query->where('restaurant_id', $restaurantId);
            })
            ->when($request->has('plates'), function ($query) {
                return $query->with('plates');
            })
            ->when($request->has('ready'), function ($query) use ($ready) {
                return $query->where('ready', $ready);
            })
            ->when($request->has('start_date'), function ($query) use ($start_date) {
                return $query->whereDate('created_at', '>=', $start_date);
            })
            ->when($request->has('end_date'), function ($query) use ($end_date) {
                return $query->whereDate('created_at', '<=', $end_date);
            })
            ->orderBy('created_at', 'DESC')
            ->get();
            return $sales;
        }
    }

    public function store(Request $request)
    {
        if ($request->has('sale_id')) {
            $sale = Sale::find($request->sale_id);
            $this->authorize('store', [Order::class, $sale]);
            $order = new Order($request->all());
            try {
                $order->save();
                if ($request->has('plates')) {
                    $platesArray = array();
                    foreach ($request->plates as $plate) {
                        $platesArray[$plate['id']] = array();
                        $platesArray[$plate['id']]['quantity'] = $plate['pivot']['quantity'];
                    }
                    $order->plates()->attach($platesArray);
                }
            } catch (\Illuminate\Database\QueryException $e) {
                dd($e);
            }
            NewOrder::dispatch($order);
            return $order;
        }
    }

    public function show($id, Request $request)
    {
        if ($request->has('plates'))
        {   
            $order = Order::with('plates')->find($id);
        } else {
            $order = Order::find($id);
        }
        $this->authorize('show', $order);
        return $order;
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $this->authorize('update', $order);
        if ($request->has('plates')) {
            $order->plates()->detach();
            foreach ($request->plates as $plate) {
                $quantity = $plate['pivot']['quantity'];
                $price = $plate['price'];
                $order->plates()->attach([$plate['id'] => ['quantity' => $quantity, 'price' => $price]]);
            }
        }
        if ($request->ready && !$order->ready) {
            OrderReady::dispatch($order);
        }
        $order->fill($request->all());
        $order->save();
    }

    public function destroy(Order $order)
    {
        $this->authorize('destroy', $order);
        $order->delete();
    }

}
