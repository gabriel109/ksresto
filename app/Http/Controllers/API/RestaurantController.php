<?php

namespace App\Http\Controllers\API;

use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sale;
use DB;
use Illuminate\Support\Facades\Auth;

class RestaurantController extends Controller
{

    public function index()
    {
        return Auth::user()->restaurant;
    }

    public function store(Request $request)
    {
        $this->authorize('store', Restaurant::class);
        $restaurant = new Restaurant();
        $restaurant->name = $request->name;
        $restaurant->owner_id = auth()->id();
        $restaurant->save();
        return $restaurant;
    }

    public function show($id)
    {
        $restaurant = Restaurant::find($id);
        $this->authorize('show', $restaurant);
        return $restaurant;
    }

    public function update(Request $request, $id)
    {
        $restaurant = Restaurant::find($id);
        $this->authorize('update', $restaurant);
        $restaurant->name = $request->name;
        $restaurant->save();
    }

    public function destroy(Restaurant $restaurant)
    {
        $this->authorize('destroy', $restaurant);
        $restaurant->delete();
    }

    public function users(Request $request, $id)
    {
        $restaurant = Restaurant::find($id);
        $this->authorize('show', $restaurant);
        return $restaurant->users()->get();
    }

    public function reportSales(Request $request, Restaurant $restaurant)
    {
        $numRows = $request->numRows;
        $sales = Sale::where('restaurant_id', $restaurant->id)
            ->where('paid', 1)
            ->whereDate('created_at', '>=', $request->start_date)
            ->whereDate('created_at', '<=', $request->end_date)
            ->selectRaw('DATE(created_at) as date, sum(amount) as income')
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy('date', 'desc')
            ->when($numRows, function ($q) use ($numRows) {
                return $q->paginate($numRows);
            }, function ($q) {
                return $q->get();
            });
        return $sales;
    }
}
