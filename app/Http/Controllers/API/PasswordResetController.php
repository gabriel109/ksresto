<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\User;
use App\PasswordReset;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user)
            return response()->json(['message' => "No pudimos encontrar un usuario con el correo indicado."], 404);
        $user->token_created = Carbon::now();
        $user->token = str_random(60);
        $user->save();
        $user->notify(new PasswordResetRequest($user->token));

        return response()->json([
            'message' => 'Te enviamos un correo para cambiar la contraseña!'
        ]);
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] user object
     */
    public function find($token)
    {
        $user = User::where('token', $token)->first();

        if (!$user)
            return response()->json([
                'message' => "El token es inválido."
            ], 404);

        if (Carbon::parse($user->created_token)->addMinutes(720)->isPast()) {
            $user->token = null;
            $user->save();
            return response()->json([
                'message' => "El token es inválido."
            ], 404);
        }

        return response()->json($user);
    }

     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'token' => 'required|string'
        ]);

        $user = User::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if (!$user)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);

        $user->password = bcrypt($request->password);
        $user->token = null;
        $user->save();

        // $user->notify(new PasswordResetSuccess($passwordReset));
        return response()->json($user);
    }
}