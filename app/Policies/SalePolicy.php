<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Sale;
use App\Table;
use App\Restaurant;

class SalePolicy
{
    use HandlesAuthorization;

    public function index(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function show(User $user, Sale $sale)
    {
        return $user->restaurant_id == $sale->restaurant->id;
    }

    public function store(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function update(User $user, Sale $sale)
    {
        return $user->restaurant_id == $sale->restaurant->id;
    }

    public function destroy(User $user, Sale $sale)
    {
        return $user->restaurant_id == $sale->restaurant->id;
    }

    public function restore(User $user, Sale $sale)
    {
        //
    }

    public function forceDelete(User $user, Sale $sale)
    {
        //
    }
}
