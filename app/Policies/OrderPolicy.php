<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Sale;
use App\Order;
use App\Restaurant;

class OrderPolicy
{
    use HandlesAuthorization;

    public function index(User $user, Sale $sale)
    {
        return $user->restaurant_id == $sale->restaurant_id;
    }

    public function indexByRestaurant(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function show(User $user, Order $order)
    {
        return $user->restaurant_id == $order->sale->restaurant_id;
    }

    public function store(User $user, Sale $sale)
    {
        return $user->restaurant_id == $sale->restaurant_id;
    }

    public function update(User $user, Order $order)
    {
        return $user->restaurant_id == $order->sale->restaurant_id;
    }

    public function destroy(User $user, Order $order)
    {
        return $user->restaurant_id == $order->sale->restaurant_id;
    }

    public function restore(User $user, Order $order)
    {
        //
    }

    public function forceDelete(User $user, Order $order)
    {
        //
    }
}
