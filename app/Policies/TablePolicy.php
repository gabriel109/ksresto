<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Table;
use App\Restaurant;

class TablePolicy
{
    use HandlesAuthorization;

    public function index(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function show(User $user, Table $table)
    {
        return $user->restaurant_id == $table->restaurant->id;
    }

    public function store(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function update(User $user, Table $table)
    {
        return $user->restaurant_id == $table->restaurant->id;
    }

    public function destroy(User $user, Table $table)
    {
        return $user->restaurant_id == $table->id;
    }

    public function restore(User $user, Table $table)
    {
        //
    }

    public function forceDelete(User $user, Table $table)
    {
        //
    }
}
