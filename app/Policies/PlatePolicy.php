<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Plate;
use App\Restaurant;

class PlatePolicy
{
    use HandlesAuthorization;

    public function index(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function show(User $user, Plate $plate)
    {
        return $user->restaurant_id == $plate->restaurant->id;
    }

    public function store(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function update(User $user, Plate $plate)
    {
        return $user->restaurant_id == $plate->restaurant->id;
    }

    public function destroy(User $user, Plate $plate)
    {
        return $user->restaurant_id == $plate->restaurant->id;
    }

    public function restore(User $user, Plate $plate)
    {
        //
    }

    public function forceDelete(User $user, Plate $plate)
    {
        //
    }
}
