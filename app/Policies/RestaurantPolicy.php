<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Restaurant;

class RestaurantPolicy
{
    use HandlesAuthorization;

    public function show(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function store(User $user)
    {
        return true;
    }

    public function update(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function destroy(User $user, Restaurant $restaurant)
    {
        return $user->restaurant_id == $restaurant->id;
    }

    public function restore(User $user, Restaurant $restaurant)
    {
        //
    }

    public function forceDelete(User $user, Restaurant $restaurant)
    {
        //
    }
}
