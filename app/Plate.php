<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plate extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'restaurant_id', 'name', 'price', 'cost',
    ];

    public function orders()
    {
        return $this->belongsToMany('App\Order')
            ->using(OrderPlate::class)
            ->withPivot('quantity', 'price');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }
}
