<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'current_sale', 'restaurant_id',
    ];

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }
}
