<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'amount', 'restaurant_id', 'table_id', 'paid', 'name', 'address', 'telephone',
    ];

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }

    public function table()
    {
        return $this->belongsTo('App\Table');
    }
    
    public function orders()
    {
        return $this->hasMany('App\Order')->orderBy('created_at', 'DESC');
    }
}
