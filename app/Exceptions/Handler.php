<?php

namespace App\Exceptions;

Use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof TokenExpiredException) {
            return Response::json(['error' => 'Token Expired'],
             $exception->getStatusCode());
        } else if ($exception instanceof TokenInvalidException) {
            return Response::json(['error' => 'Token Invalid'],
             $exception->getStatusCode());            
        } else if ($exception instanceof JWTException) {
            return Response::json(['error' => 'Error fetching Token'],
             $exception->getStatusCode());               
        }
        return parent::render($request, $exception);
    }
}
