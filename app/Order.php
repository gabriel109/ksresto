<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'sale_id', 'comments', 'ready'
    ];

    public function sale()
    {
        return $this->belongsTo('App\Sale');
    }

    public function plates()
    {
        return $this->belongsToMany('App\Plate')
            ->using(OrderPlate::class)
            ->withPivot(['quantity', 'price']);
    }
}
