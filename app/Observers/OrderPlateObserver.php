<?php

namespace App\Observers;

use App\OrderPlate;

class OrderPlateObserver
{
    /**
     * Handle the order plate "creating" event.
     *
     * @param  \App\OrderPlate  $orderPlate
     * @return void
     */
    public function creating(OrderPlate $orderPlate)
    {
        $orderPlate->price = $orderPlate->plate->price;
    }

    /**
     * Handle the order plate "created" event.
     *
     * @param  \App\OrderPlate  $orderPlate
     * @return void
     */
    public function created(OrderPlate $orderPlate)
    {
        //
    }

    /**
     * Handle the order plate "updated" event.
     *
     * @param  \App\OrderPlate  $orderPlate
     * @return void
     */
    public function updated(OrderPlate $orderPlate)
    {
        //
    }

    /**
     * Handle the order plate "deleted" event.
     *
     * @param  \App\OrderPlate  $orderPlate
     * @return void
     */
    public function deleted(OrderPlate $orderPlate)
    {
        //
    }

    /**
     * Handle the order plate "restored" event.
     *
     * @param  \App\OrderPlate  $orderPlate
     * @return void
     */
    public function restored(OrderPlate $orderPlate)
    {
        //
    }

    /**
     * Handle the order plate "force deleted" event.
     *
     * @param  \App\OrderPlate  $orderPlate
     * @return void
     */
    public function forceDeleted(OrderPlate $orderPlate)
    {
        //
    }
}
