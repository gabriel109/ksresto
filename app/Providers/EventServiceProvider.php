<?php

namespace App\Providers;

use App\Observers\OrderPlateObserver;
use App\OrderPlate;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\OrderStoredEvent' => [
            'App\Listeners\OrderStoredListener',
        ],
        'App\Events\OrderReadyEvent' => [
            'App\Listeners\OrderReadyListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        OrderPlate::observe(OrderPlateObserver::class);
        //
    }
}
