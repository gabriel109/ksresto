<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Restaurant;
use App\Policies\RestaurantPolicy;
use App\Table;
use App\Policies\TablePolicy;
use App\Sale;
use App\Policies\SalePolicy;
use App\Order;
use App\Policies\OrderPolicy;
use App\Plate;
use App\Policies\PlatePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Restaurant::class => RestaurantPolicy::class,
        Table::class => TablePolicy::class,
        Sale::class => SalePolicy::class,
        Order::class => OrderPolicy::class,
        Plate::class => PlatePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        
        $this->registerPolicies();

        //
    }
}
