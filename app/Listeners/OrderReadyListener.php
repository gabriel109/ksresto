<?php

namespace App\Listeners;

use App\Events\OrderReadyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderReadyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderReadyEvent  $event
     * @return void
     */
    public function handle(OrderReadyEvent $event)
    {
        //
    }
}
