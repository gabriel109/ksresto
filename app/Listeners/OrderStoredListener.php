<?php

namespace App\Listeners;

use App\Events\OrderStoredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderStoredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderStoredEvent  $event
     * @return void
     */
    public function handle(OrderStoredEvent $event)
    {
        //
    }
}
