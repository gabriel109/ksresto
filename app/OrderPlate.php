<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderPlate extends Pivot
{
    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function plate()
    {
        return $this->belongsTo(Plate::class);
    }
}
