<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    public $timestamps = false;
    
    public function tables()
    {
        return $this->hasMany('App\Table');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale')->orderBy('created_at');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }
    
    public function plates()
    {
        return $this->hasMany('App\Plate');
    }

}
