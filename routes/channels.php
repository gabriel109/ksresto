<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
use App\Restaurant;

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('orders.{restaurant}', function ($user, Restaurant $restaurant) {
    return $restaurant->owner_id == $user->id || $user->workIn($restaurant->id);
});

Broadcast::channel('ordersReady.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});