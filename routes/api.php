<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// For auth
Route::post('auth/register', 'API\UserController@register');
Route::post('auth/login', 'API\UserController@login')->name('login');
Route::post('auth/password/create', 'API\PasswordResetController@create');
Route::get('auth/password/find/{token}', 'API\PasswordResetController@find');
Route::post('auth/password/reset', 'API\PasswordResetController@reset');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('auth/user', 'API\UserController@user');
    Route::put('auth/user', 'API\UserController@update');
    Route::put('auth/user/service', 'API\UserController@putService');
    Route::post('auth/logout', 'API\UserController@logout');
});
Route::group(['middleware' => 'jwt.refresh'], function () {
    Route::get('auth/refresh', 'API\UserController@refresh');
});

// Guest
Route::get('services', 'API\ServiceController@index');
Route::get('services/{service}', 'API\ServiceController@show');

// Authenticated
Route::middleware('jwt.auth')->group(function () {
    Route::apiResource('restaurants', 'API\RestaurantController');
    Route::apiResource('tables', 'API\TableController');
    Route::apiResource('sales', 'API\SaleController');
    Route::apiResource('plates', 'API\PlateController');
    Route::apiResource('orders', 'API\OrderController');

    Route::post('orders/{order}/plates/{array}', 'API\OrderController@attachPlates');
    Route::delete('orders/{order}/plates/{array}', 'API\OrderController@detachPlates');
    Route::get('restaurants/{restaurant}/users', 'API\RestaurantController@users');

    Route::get('restaurants/{restaurant}/reportSales', 'API\RestaurantController@reportSales');
});
