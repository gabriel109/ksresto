
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';
import VueRouter from 'vue-router';
import moment from 'moment';

moment.locale('es');

Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import store from './store.js';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons/faPlusCircle';
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons/faArrowCircleLeft';
import { faUtensils } from '@fortawesome/free-solid-svg-icons/faUtensils';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons/faCircleNotch';
import { faHeart } from '@fortawesome/free-solid-svg-icons/faHeart';
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons/faMobileAlt';
import { faBookOpen } from '@fortawesome/free-solid-svg-icons/faBookOpen';
import { faConciergeBell } from '@fortawesome/free-solid-svg-icons/faConciergeBell';
import { faChartLine } from '@fortawesome/free-solid-svg-icons/faChartLine';
import { faEdit } from '@fortawesome/free-solid-svg-icons/faEdit';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faBell } from '@fortawesome/free-solid-svg-icons/faBell';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Notifications from 'vue-notification'

import App from './components/App';
import TheNavbar from './components/TheNavbar';
import Button from './components/Shared/Button';

Vue.component('the-navbar', TheNavbar);
Vue.component('pagination', require('laravel-vue-pagination'));
library.add(faPlusCircle, faArrowCircleLeft, faUtensils, faCircleNotch, faHeart, 
    faMobileAlt, faBookOpen, faConciergeBell, faChartLine, faEdit, faTimes, faBell);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('v-button', Button);
Vue.use(Notifications);

// Lazy loading
const Home = () => import('./components/Home');
const Login = () => import('./components/Auth/Login');
const Logout = () => import('./components/Auth/Logout');
const Register = () => import('./components/Auth/Register');
const PasswordEmail = () => import('./components/Auth/PasswordEmail');
const PasswordReset = () => import('./components/Auth/PasswordReset');
const MyRestaurants = () => import('./components/Restaurant/MyRestaurants');
const Settings = () => import('./components/Restaurant/Settings');
const MyTables = () => import('./components/Table/MyTables');
const NotFound = () => import('./components/NotFound');
const MyOrders = () => import('./components/Order/MyOrders');
const MyPlates = () => import('./components/Plate/MyPlates');
const Services = () => import('./components/Payment/Services');
const ReportSales = () => import('./components/Restaurant/ReportSales');

const routes = [
    { path: '/restaurants', name: 'restaurants', component: MyRestaurants, meta: {requiresAuth: true} },
    { path: '/restaurants/:id/tables', name: 'tables', component: MyTables, props: true, meta: {requiresAuth: true} },
    { path: '/restaurants/:id/orders', name: 'orders', component: MyOrders, props: true, meta: {requiresAuth: true} },
    { path: '/restaurants/:id/plates', name: 'plates', component: MyPlates, props: true, meta: {requiresAuth: true} },
    { path: '/restaurants/:id/settings', name: 'res-settings', component: Settings, props: true, meta: {requiresAuth: true} },
    { path: '/restaurants/:id/reportSales', name: 'report-sales', component: ReportSales, props: true, meta: {requiresAuth: true} },
    { path: '/services', name: 'services', component: Services },
    { path: '/', name: 'home', component: Home},
    { path: '/login', name: 'login', component: Login},
    { path: '/logout', name: 'logout', component: Logout},
    { path: '/register', name: 'register', component: Register},
    { path: '/password_email', name: 'passwordEmail', component: PasswordEmail},
    { path: '/password_reset/:token', name: 'passwordReset', component: PasswordReset, props: true},
    { path: '/notFound', name: 'notFound', component: NotFound },
    { path: '*', redirect: { name: 'notFound' } }
];

const router = new VueRouter({
    mode: 'history',
    routes
});


router.beforeEach((to, from, next) => {
    NProgress.start()
    // check if the route requires authentication and user is not logged in
    if (to.matched.some(route => route.meta.requiresAuth) && !store.state.authenticated) {
        next({ name: 'login' })
        return
    }

    // if logged in redirect to dashboard
    if(to.path === '/login' && store.state.authenticated) {
        next({ name: 'restaurants' })
        return
    }

    next()
});

router.afterEach((to, from) => {
    NProgress.done()
});

NProgress.configure({ showSpinner: false })

// set header token for all axios request
axios.interceptors.request.use(
    (config) => {
      if (store.state.authenticated) {
        config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
      }
  
      return config;
    }, 
  
    (error) => {
      return Promise.reject(error);
    }
);



axios.interceptors.response.use((response) => { // intercept the global error
    return response
  }, function (error) {
    let originalRequest = error.config
    if (error.response.status === 401 && error.config && !originalRequest._retry && store.state.authenticated) { // if the error is 401 and hasnt already been retried
        originalRequest._retry = true // now it can be retried 
        return new Promise((resolve, reject) => {
            axios.get('/api/auth/refresh').then((data) => {
                var token = data.headers['authorization'].slice(7);
                localStorage.setItem('token', token)
                store.commit('loginUser')
                originalRequest.headers['Authorization'] = 'Bearer ' + token // new header new token
                resolve(axios(originalRequest)) // retry the request that errored out
            }).catch((error) => {
                router.push({ name: 'logout' })
            })
        });
    }
    // Do something with response error
    return Promise.reject(error)
  })


Vue.mixin({
    methods: {
        onlyTime(date){
            if(date) {
                return moment(date).calendar();
            }
        }
    }
})


const app = new Vue({
    el: '#app',
    store,
    components: { App },
    router
});
