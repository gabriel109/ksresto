import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        authenticated: !!localStorage.getItem('token'),
        userId: localStorage.getItem('userId')
    },
    mutations: {
        loginUser (state, userId) {
            state.authenticated = true;
            state.userId = userId;
        },
        logoutUser (state) {
            state.authenticated = false;
            state.userId = null;
        },
    },
    getters: {}
})