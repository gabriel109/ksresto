webpackJsonp([25],{

/***/ 100:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(true);
// imports


// module
exports.push([module.i, "\n.form-wrapper[data-v-17ca704c] {\n    min-height: 100%;\n    min-height: 60vh;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.form-signin[data-v-17ca704c] {\n    width: 100%;\n    max-width: 330px;\n    padding: 15px;\n    margin: 0 auto;\n}\n.form-signin .form-control[data-v-17ca704c] {\n    position: relative;\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n    height: auto;\n    padding: 10px;\n    font-size: 16px;\n}\n.form-signin .form-control[data-v-17ca704c]:focus {\n    z-index: 2;\n}\n.form-signin input[type=\"email\"][data-v-17ca704c] {\n    margin-bottom: -1px;\n    border-bottom-right-radius: 0;\n    border-bottom-left-radius: 0;\n}\n.form-signin input[type=\"password\"][data-v-17ca704c] {\n    margin-bottom: 10px;\n    border-top-left-radius: 0;\n    border-top-right-radius: 0;\n}\n", "", {"version":3,"sources":["C:/Apache24/htdocs/ksresto/resources/assets/js/components/Auth/resources/assets/js/components/Auth/Login.vue"],"names":[],"mappings":";AA8EA;IACA,iBAAA;IACA,iBAAA;IACA,qBAAA;IAAA,qBAAA;IAAA,cAAA;IACA,0BAAA;QAAA,uBAAA;YAAA,oBAAA;CACA;AACA;IACA,YAAA;IACA,iBAAA;IACA,cAAA;IACA,eAAA;CACA;AACA;IACA,mBAAA;IACA,+BAAA;YAAA,uBAAA;IACA,aAAA;IACA,cAAA;IACA,gBAAA;CACA;AACA;IACA,WAAA;CACA;AACA;IACA,oBAAA;IACA,8BAAA;IACA,6BAAA;CACA;AACA;IACA,oBAAA;IACA,0BAAA;IACA,2BAAA;CACA","file":"Login.vue","sourcesContent":["<template>\r\n    <div class=\"text-center form-wrapper\">\r\n\r\n        <form class=\"form-signin\" v-on:submit.prevent=\"\">\r\n            <img class=\"mb-4\" src=\"https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg\" alt=\"\" width=\"72\" height=\"72\">\r\n            <h1 class=\"h3 mb-3 font-weight-normal\">Ingresa a KSResto</h1>\r\n\r\n            <label for=\"inputEmail\" class=\"sr-only\">Email</label>\r\n            <input type=\"email\" id=\"inputEmail\" class=\"form-control\" placeholder=\"Email\" required autofocus v-model=\"email\" autofocus>\r\n\r\n            <label for=\"inputPassword\" class=\"sr-only\">Contraseña</label>\r\n            <input type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"Contraseña\" required v-model=\"password\">\r\n            <v-button :showIcon=\"isLoading\" @clicked=\"login\" class=\"btn-block\" style=\"font-size: 1.125rem;\">Ingresar</v-button>           \r\n            <div v-if=\"errors.length\" class=\"alert alert-danger\" role=\"alert\" style=\"margin-top:10px\">\r\n                <li v-for=\"error in errors\">{{ error }}</li>\r\n            </div>\r\n            <br />\r\n            <router-link :to=\"{name: 'passwordEmail'}\">¿Olvidaste tu contraseña?</router-link>\r\n        </form>\r\n        <br />\r\n\r\n    </div>\r\n</template>\r\n\r\n<script>\r\nimport Echo from 'laravel-echo';\r\n\r\n    export default {\r\n        data() {\r\n            return {\r\n                email: '',\r\n                password: '',\r\n                isLoading: false,\r\n                errors: []\r\n            }\r\n        },\r\n        methods: {\r\n            login() {\r\n                this.isLoading = true;\r\n                this.errors = [];\r\n                axios.post('/api/auth/login', {\r\n                    email: this.email,\r\n                    password: this.password\r\n                }).then(response => {\r\n                    // Store token\r\n                    localStorage.setItem('token', response.data.token);\r\n                    // Get payload from token\r\n                    var base64Url = response.data.token.split('.')[1];\r\n                    var base64 = base64Url.replace('-', '+').replace('_', '/');\r\n                    var tokenPayload = JSON.parse(window.atob(base64));\r\n                    // Store user id\r\n                    localStorage.setItem('userId', tokenPayload.sub);\r\n                    this.$store.commit('loginUser', tokenPayload.sub);\r\n                    axios.get('/api/auth/user/permissions').then((response) => {\r\n                        // Store permissions\r\n                        localStorage.setItem('permissions', JSON.stringify(response.data));\r\n                        this.$router.push(this.$route.query.redirect || { name: 'restaurants' })\r\n                    });\r\n                    // Change auth for pusher notifications\r\n                    window.Echo.connector.pusher.config.auth.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');\r\n                }).catch(error => {\r\n                    this.isLoading = false;\r\n                    if (error.response.status == 401) {\r\n                        this.errors.push(error.response.data.msg);\r\n                    }   \r\n                    let errores = error.response.data.errors;\r\n                    Object.keys(errores).forEach((item, index) => {\r\n                        this.errors.push(errores[item][0]);\r\n                    });\r\n                    \r\n                 \r\n                });\r\n            }\r\n        }\r\n    }\r\n</script>\r\n\r\n<style scoped>\r\n    .form-wrapper {\r\n        min-height: 100%;\r\n        min-height: 60vh;\r\n        display: flex;\r\n        align-items: center;\r\n    }\r\n    .form-signin {\r\n        width: 100%;\r\n        max-width: 330px;\r\n        padding: 15px;\r\n        margin: 0 auto;\r\n    }\r\n    .form-signin .form-control {\r\n        position: relative;\r\n        box-sizing: border-box;\r\n        height: auto;\r\n        padding: 10px;\r\n        font-size: 16px;\r\n    }\r\n    .form-signin .form-control:focus {\r\n        z-index: 2;\r\n    }\r\n    .form-signin input[type=\"email\"] {\r\n        margin-bottom: -1px;\r\n        border-bottom-right-radius: 0;\r\n        border-bottom-left-radius: 0;\r\n    }\r\n    .form-signin input[type=\"password\"] {\r\n        margin-bottom: 10px;\r\n        border-top-left-radius: 0;\r\n        border-top-right-radius: 0;\r\n    }\r\n</style>"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_laravel_echo__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_laravel_echo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_laravel_echo__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            email: '',
            password: '',
            isLoading: false,
            errors: []
        };
    },

    methods: {
        login: function login() {
            var _this = this;

            this.isLoading = true;
            this.errors = [];
            axios.post('/api/auth/login', {
                email: this.email,
                password: this.password
            }).then(function (response) {
                // Store token
                localStorage.setItem('token', response.data.token);
                // Get payload from token
                var base64Url = response.data.token.split('.')[1];
                var base64 = base64Url.replace('-', '+').replace('_', '/');
                var tokenPayload = JSON.parse(window.atob(base64));
                // Store user id
                localStorage.setItem('userId', tokenPayload.sub);
                _this.$store.commit('loginUser', tokenPayload.sub);
                axios.get('/api/auth/user/permissions').then(function (response) {
                    // Store permissions
                    localStorage.setItem('permissions', JSON.stringify(response.data));
                    _this.$router.push(_this.$route.query.redirect || { name: 'restaurants' });
                });
                // Change auth for pusher notifications
                window.Echo.connector.pusher.config.auth.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
            }).catch(function (error) {
                _this.isLoading = false;
                if (error.response.status == 401) {
                    _this.errors.push(error.response.data.msg);
                }
                var errores = error.response.data.errors;
                Object.keys(errores).forEach(function (item, index) {
                    _this.errors.push(errores[item][0]);
                });
            });
        }
    }
});

/***/ }),

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "text-center form-wrapper" }, [
    _c(
      "form",
      {
        staticClass: "form-signin",
        on: {
          submit: function($event) {
            $event.preventDefault()
          }
        }
      },
      [
        _c("img", {
          staticClass: "mb-4",
          attrs: {
            src:
              "https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg",
            alt: "",
            width: "72",
            height: "72"
          }
        }),
        _vm._v(" "),
        _c("h1", { staticClass: "h3 mb-3 font-weight-normal" }, [
          _vm._v("Ingresa a KSResto")
        ]),
        _vm._v(" "),
        _c("label", { staticClass: "sr-only", attrs: { for: "inputEmail" } }, [
          _vm._v("Email")
        ]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.email,
              expression: "email"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "email",
            id: "inputEmail",
            placeholder: "Email",
            required: "",
            autofocus: "",
            autofocus: ""
          },
          domProps: { value: _vm.email },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.email = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _c(
          "label",
          { staticClass: "sr-only", attrs: { for: "inputPassword" } },
          [_vm._v("Contraseña")]
        ),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.password,
              expression: "password"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "password",
            id: "inputPassword",
            placeholder: "Contraseña",
            required: ""
          },
          domProps: { value: _vm.password },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.password = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _c(
          "v-button",
          {
            staticClass: "btn-block",
            staticStyle: { "font-size": "1.125rem" },
            attrs: { showIcon: _vm.isLoading },
            on: { clicked: _vm.login }
          },
          [_vm._v("Ingresar")]
        ),
        _vm._v(" "),
        _vm.errors.length
          ? _c(
              "div",
              {
                staticClass: "alert alert-danger",
                staticStyle: { "margin-top": "10px" },
                attrs: { role: "alert" }
              },
              _vm._l(_vm.errors, function(error) {
                return _c("li", [_vm._v(_vm._s(error))])
              })
            )
          : _vm._e(),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("router-link", { attrs: { to: { name: "passwordEmail" } } }, [
          _vm._v("¿Olvidaste tu contraseña?")
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c("br")
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-17ca704c", module.exports)
  }
}

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(99)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(101)
/* template */
var __vue_template__ = __webpack_require__(102)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-17ca704c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\Auth\\Login.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-17ca704c", Component.options)
  } else {
    hotAPI.reload("data-v-17ca704c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 99:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(100);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(6)("562ba0af", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-17ca704c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Login.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-17ca704c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Login.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});
//# sourceMappingURL=25.js.map