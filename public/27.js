webpackJsonp([27],{

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            bills: [],
            isLoading: false
        };
    },

    methods: {
        pay: function pay(bill) {
            this.isLoading = true;
            var params = {
                'bill_id': bill.id
            };
            axios.post('/api/payment/generatePaymentGateway', params).then(function (response) {
                window.location.href = response.data;
            });
        }
    },
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        axios.get('/api/bills').then(function (response) {
            next(function (vm) {
                return vm.bills = response.data;
            });
        });
    }
});

/***/ }),

/***/ 168:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "card-body" }, [
      _c("table", { staticClass: "table table-hover" }, [
        _vm._m(1),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.bills, function(bill) {
            return _c(
              "tr",
              {
                key: bill.id,
                class: { "table-success": bill.status == "approved" }
              },
              [
                _c("td", [_vm._v(_vm._s(bill.service.name))]),
                _vm._v(" "),
                bill.status == "approved"
                  ? _c("td", [_vm._v("Pagada")])
                  : bill.status == "in_process"
                    ? _c("td", [_vm._v("En proceso")])
                    : bill.status == "pending"
                      ? _c("td", [_vm._v("Pendiente")])
                      : _c("td", [_vm._v(_vm._s(bill.status))]),
                _vm._v(" "),
                _c("td", [_vm._v(_vm._s(bill.date_expiration))]),
                _vm._v(" "),
                _c("td", { attrs: { scope: "row" } }, [
                  _vm._v("$" + _vm._s(bill.amount))
                ]),
                _vm._v(" "),
                _c(
                  "td",
                  [
                    bill.status != "approved" &&
                    bill.status != "pending" &&
                    bill.status != "in_process"
                      ? _c(
                          "v-button",
                          {
                            staticClass: "btn btn-success",
                            attrs: { showIcon: _vm.isLoading },
                            on: {
                              clicked: function($event) {
                                _vm.pay(bill)
                              }
                            }
                          },
                          [_vm._v("\r\n                Pagar")]
                        )
                      : _vm._e()
                  ],
                  1
                )
              ]
            )
          })
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h5", { staticStyle: { float: "left", "padding-top": "5px" } }, [
        _vm._v("Mis Facturas")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Servicio")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Estado")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Vencimiento")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Monto")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Pagar")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-89d0203a", module.exports)
  }
}

/***/ }),

/***/ 90:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(167)
/* template */
var __vue_template__ = __webpack_require__(168)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\Payment\\MyBills.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-89d0203a", Component.options)
  } else {
    hotAPI.reload("data-v-89d0203a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});
//# sourceMappingURL=27.js.map