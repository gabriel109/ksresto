webpackJsonp([24],{

/***/ 105:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(106);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(6)("5da06b8c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4d533510\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Register.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4d533510\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Register.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 106:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(true);
// imports


// module
exports.push([module.i, "\n.form-wrapper[data-v-4d533510] {\n    min-height: 100%;\n    min-height: 60vh;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.form-signin[data-v-4d533510] {\n    width: 100%;\n    max-width: 330px;\n    padding: 15px;\n    margin: 0 auto;\n}\n.form-signin .form-control[data-v-4d533510] {\n    position: relative;\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n    height: auto;\n    padding: 10px;\n    font-size: 16px;\n}\n.form-signin .form-control[data-v-4d533510]:focus {\n    z-index: 2;\n}\n.form-signin input[data-v-4d533510]:first-of-type {\n    margin-bottom: -1px;\n    border-bottom-right-radius: 0;\n    border-bottom-left-radius: 0;\n}\n.form-signin input[data-v-4d533510]:last-of-type {\n    margin-bottom: 10px;\n    border-top-left-radius: 0;\n    border-top-right-radius: 0;\n}\n.form-signin input[data-v-4d533510]:not(:first-of-type):not(:last-of-type) {\n    margin-bottom: -1px;\n    border-radius: 0;\n}\n\n", "", {"version":3,"sources":["C:/Apache24/htdocs/ksresto/resources/assets/js/components/Auth/resources/assets/js/components/Auth/Register.vue"],"names":[],"mappings":";AAiFA;IACA,iBAAA;IACA,iBAAA;IACA,qBAAA;IAAA,qBAAA;IAAA,cAAA;IACA,0BAAA;QAAA,uBAAA;YAAA,oBAAA;CACA;AACA;IACA,YAAA;IACA,iBAAA;IACA,cAAA;IACA,eAAA;CACA;AACA;IACA,mBAAA;IACA,+BAAA;YAAA,uBAAA;IACA,aAAA;IACA,cAAA;IACA,gBAAA;CACA;AACA;IACA,WAAA;CACA;AAEA;IACA,oBAAA;IACA,8BAAA;IACA,6BAAA;CACA;AACA;IACA,oBAAA;IACA,0BAAA;IACA,2BAAA;CACA;AACA;IACA,oBAAA;IACA,iBAAA;CACA","file":"Register.vue","sourcesContent":["<template>\r\n    <div class=\"text-center form-wrapper\">\r\n        <form class=\"form-signin\" v-on:submit.prevent=\"\">\r\n            <img class=\"mb-4\" src=\"https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg\" alt=\"\" width=\"72\" height=\"72\">\r\n            <h1 class=\"h3 mb-3 font-weight-normal\">Registro</h1>\r\n\r\n            <label for=\"name\" class=\"sr-only\">Nombre y Apellido</label>\r\n            <input type=\"text\" id=\"name\" class=\"form-control\" placeholder=\"Nombre y Apellido\" required autofocus v-model=\"name\" autofocus>\r\n\r\n            <label for=\"email\" class=\"sr-only\">Email</label>\r\n            <input type=\"email\" id=\"email\" class=\"form-control\" placeholder=\"Email\" required autofocus v-model=\"email\">\r\n\r\n            <label for=\"password\" class=\"sr-only\">Contraseña</label>\r\n            <input type=\"password\" id=\"password\" class=\"form-control\" placeholder=\"Contraseña\" required v-model=\"password\">\r\n\r\n            <label for=\"passwordConfirmation\" class=\"sr-only\">Confirmar Contraseña</label>\r\n            <input type=\"password\" id=\"passwordConfirmation\" class=\"form-control\" placeholder=\"Confirmación de contraseña\" required v-model=\"passwordConfirmation\">\r\n\r\n            <v-button :showIcon=\"isLoading\" @clicked=\"register\" class=\"btn-block\" style=\"font-size: 1.125rem;\">Registrarme</v-button>\r\n            <br />\r\n            <div v-if=\"errors.length\">\r\n                <b>Por favor corrige los siguientes errore(s):</b>\r\n                <li v-for=\"error in errors\">{{ error }}</li>\r\n            </div>\r\n        </form>\r\n\r\n    </div>\r\n</template>\r\n\r\n<script>\r\nexport default {\r\n    data() {\r\n        return {\r\n            email: '',\r\n            password: '',\r\n            passwordConfirmation: '',\r\n            name: '',\r\n            isLoading: false,\r\n            errors: [],\r\n        }\r\n    },\r\n    methods: {\r\n        register() {\r\n            if(!this.checkErrors()) return false;\r\n            this.isLoading = true;\r\n            axios.post('/api/auth/register', {\r\n                email: this.email,\r\n                password: this.password,\r\n                name: this.name\r\n            }).then(response => {\r\n                localStorage.setItem('token', response.data.token)\r\n                // Get payload from token\r\n                var base64Url = response.data.token.split('.')[1];\r\n                var base64 = base64Url.replace('-', '+').replace('_', '/');\r\n                var tokenPayload = JSON.parse(window.atob(base64));   \r\n                //\r\n                localStorage.setItem('userId', tokenPayload.sub);\r\n                this.$store.commit('loginUser', tokenPayload.sub);\r\n                this.$router.push({ name: 'restaurants' });        \r\n            }).catch(error =>{\r\n                let errores = error.response.data.errors;\r\n                Object.keys(errores).forEach((item, index) => {\r\n                    this.errors.push(errores[item][0]);\r\n                });\r\n                this.isLoading = false;\r\n            });\r\n        },\r\n        checkErrors() {\r\n            this.errors = [];\r\n            if(this.password != this.passwordConfirmation) {\r\n                this.errors = [];\r\n                this.errors.push('Las contraseñas no coinciden.');\r\n                return false;\r\n            }\r\n            return true;\r\n        },\r\n    }\r\n}\r\n</script>\r\n\r\n<style scoped>\r\n    .form-wrapper {\r\n        min-height: 100%;\r\n        min-height: 60vh;\r\n        display: flex;\r\n        align-items: center;\r\n    }\r\n    .form-signin {\r\n        width: 100%;\r\n        max-width: 330px;\r\n        padding: 15px;\r\n        margin: 0 auto;\r\n    }\r\n    .form-signin .form-control {\r\n        position: relative;\r\n        box-sizing: border-box;\r\n        height: auto;\r\n        padding: 10px;\r\n        font-size: 16px;\r\n    }\r\n    .form-signin .form-control:focus {\r\n        z-index: 2;\r\n    }\r\n\r\n    .form-signin input:first-of-type {\r\n        margin-bottom: -1px;\r\n        border-bottom-right-radius: 0;\r\n        border-bottom-left-radius: 0;\r\n    }\r\n    .form-signin input:last-of-type {\r\n        margin-bottom: 10px;\r\n        border-top-left-radius: 0;\r\n        border-top-right-radius: 0;\r\n    }\r\n    .form-signin input:not(:first-of-type):not(:last-of-type) {\r\n        margin-bottom: -1px;\r\n        border-radius: 0;\r\n    }\r\n\r\n</style>\r\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            email: '',
            password: '',
            passwordConfirmation: '',
            name: '',
            isLoading: false,
            errors: []
        };
    },

    methods: {
        register: function register() {
            var _this = this;

            if (!this.checkErrors()) return false;
            this.isLoading = true;
            axios.post('/api/auth/register', {
                email: this.email,
                password: this.password,
                name: this.name
            }).then(function (response) {
                localStorage.setItem('token', response.data.token);
                // Get payload from token
                var base64Url = response.data.token.split('.')[1];
                var base64 = base64Url.replace('-', '+').replace('_', '/');
                var tokenPayload = JSON.parse(window.atob(base64));
                //
                localStorage.setItem('userId', tokenPayload.sub);
                _this.$store.commit('loginUser', tokenPayload.sub);
                _this.$router.push({ name: 'restaurants' });
            }).catch(function (error) {
                var errores = error.response.data.errors;
                Object.keys(errores).forEach(function (item, index) {
                    _this.errors.push(errores[item][0]);
                });
                _this.isLoading = false;
            });
        },
        checkErrors: function checkErrors() {
            this.errors = [];
            if (this.password != this.passwordConfirmation) {
                this.errors = [];
                this.errors.push('Las contraseñas no coinciden.');
                return false;
            }
            return true;
        }
    }
});

/***/ }),

/***/ 108:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "text-center form-wrapper" }, [
    _c(
      "form",
      {
        staticClass: "form-signin",
        on: {
          submit: function($event) {
            $event.preventDefault()
          }
        }
      },
      [
        _c("img", {
          staticClass: "mb-4",
          attrs: {
            src:
              "https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg",
            alt: "",
            width: "72",
            height: "72"
          }
        }),
        _vm._v(" "),
        _c("h1", { staticClass: "h3 mb-3 font-weight-normal" }, [
          _vm._v("Registro")
        ]),
        _vm._v(" "),
        _c("label", { staticClass: "sr-only", attrs: { for: "name" } }, [
          _vm._v("Nombre y Apellido")
        ]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.name,
              expression: "name"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "text",
            id: "name",
            placeholder: "Nombre y Apellido",
            required: "",
            autofocus: "",
            autofocus: ""
          },
          domProps: { value: _vm.name },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.name = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _c("label", { staticClass: "sr-only", attrs: { for: "email" } }, [
          _vm._v("Email")
        ]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.email,
              expression: "email"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "email",
            id: "email",
            placeholder: "Email",
            required: "",
            autofocus: ""
          },
          domProps: { value: _vm.email },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.email = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _c("label", { staticClass: "sr-only", attrs: { for: "password" } }, [
          _vm._v("Contraseña")
        ]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.password,
              expression: "password"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "password",
            id: "password",
            placeholder: "Contraseña",
            required: ""
          },
          domProps: { value: _vm.password },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.password = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _c(
          "label",
          { staticClass: "sr-only", attrs: { for: "passwordConfirmation" } },
          [_vm._v("Confirmar Contraseña")]
        ),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.passwordConfirmation,
              expression: "passwordConfirmation"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "password",
            id: "passwordConfirmation",
            placeholder: "Confirmación de contraseña",
            required: ""
          },
          domProps: { value: _vm.passwordConfirmation },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.passwordConfirmation = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _c(
          "v-button",
          {
            staticClass: "btn-block",
            staticStyle: { "font-size": "1.125rem" },
            attrs: { showIcon: _vm.isLoading },
            on: { clicked: _vm.register }
          },
          [_vm._v("Registrarme")]
        ),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _vm.errors.length
          ? _c(
              "div",
              [
                _c("b", [
                  _vm._v("Por favor corrige los siguientes errore(s):")
                ]),
                _vm._v(" "),
                _vm._l(_vm.errors, function(error) {
                  return _c("li", [_vm._v(_vm._s(error))])
                })
              ],
              2
            )
          : _vm._e()
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4d533510", module.exports)
  }
}

/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(105)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(107)
/* template */
var __vue_template__ = __webpack_require__(108)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4d533510"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\Auth\\Register.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4d533510", Component.options)
  } else {
    hotAPI.reload("data-v-4d533510", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});
//# sourceMappingURL=24.js.map