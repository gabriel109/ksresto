webpackJsonp([31],{

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c(
          "div",
          { staticStyle: { padding: "40px 15px", "text-align": "center" } },
          [
            _c("h1", [_vm._v("\n                Oops!")]),
            _vm._v(" "),
            _c("h2", [_vm._v("\n                404 Not Found")]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticStyle: { "margin-top": "15px", "margin-bottom": "15px" }
              },
              [
                _vm._v(
                  "\n                Sorry, an error has occured, Requested page not found!\n            "
                )
              ]
            ),
            _vm._v(" "),
            _c("div", { staticStyle: { "margin-right": "10px" } }, [
              _c(
                "a",
                { staticClass: "btn btn-primary btn-lg", attrs: { href: "/" } },
                [
                  _c("span", { staticClass: "glyphicon glyphicon-home" }),
                  _vm._v("\n                    Take Me Home ")
                ]
              ),
              _c(
                "a",
                { staticClass: "btn btn-default btn-lg", attrs: { href: "/" } },
                [
                  _c("span", { staticClass: "glyphicon glyphicon-envelope" }),
                  _vm._v(" Contact Support ")
                ]
              )
            ])
          ]
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-075e1f66", module.exports)
  }
}

/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(154)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\NotFound.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-075e1f66", Component.options)
  } else {
    hotAPI.reload("data-v-075e1f66", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});
//# sourceMappingURL=31.js.map