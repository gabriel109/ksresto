(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Order/MyOrders.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id'],
  //restaurant_id
  data: function data() {
    return {
      orders: [],
      periodSelected: '0',
      periods: [{
        text: 'Ultimas 24 horas',
        value: '0'
      }, {
        text: 'Ultimos 7 dias',
        value: '1'
      }, {
        text: 'Ultimos 30 dias',
        value: '2'
      }],
      stateSelected: '2',
      states: [{
        text: 'Pendientes',
        value: '0'
      }, {
        text: 'Listos',
        value: '1'
      }, {
        text: 'Todos',
        value: '2'
      }]
    };
  },
  methods: {
    setReady: function setReady(order) {
      var _this = this;

      var updOrder = {
        id: order.id,
        ready: 1
      };
      axios.put('/api/orders/' + order.id, updOrder).then(function (response) {
        _this.orders[_this.orders.indexOf(order)].ready = 1;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    onChangePeriod: function onChangePeriod(event) {
      var _this2 = this;

      var params = {
        restaurant_id: this.id,
        plates: true
      };

      switch (event.target.value) {
        case '0':
          params["start_date"] = moment__WEBPACK_IMPORTED_MODULE_0___default()(moment__WEBPACK_IMPORTED_MODULE_0___default()().subtract(1, 'days')).format("YYYY-MM-DD H:mm:ss");
          break;

        case '1':
          params["start_date"] = moment__WEBPACK_IMPORTED_MODULE_0___default()(moment__WEBPACK_IMPORTED_MODULE_0___default()().subtract(7, 'days')).format("YYYY-MM-DD");
          break;

        case '2':
          params["start_date"] = moment__WEBPACK_IMPORTED_MODULE_0___default()(moment__WEBPACK_IMPORTED_MODULE_0___default()().subtract(30, 'days')).format("YYYY-MM-DD");
          break;
      }

      console.log(params);
      axios.get('/api/orders', {
        params: params
      }).then(function (response) {
        _this2.orders = response.data;
      });
    },
    onChangeState: function onChangeState(event) {
      console.log(event.target.value);
    }
  },
  mounted: function mounted() {
    var _this3 = this;

    moment__WEBPACK_IMPORTED_MODULE_0___default.a.locale('es');
    Echo["private"]('orders.' + this.id).listen('OrderStoredEvent', function (e) {
      _this3.$notify({
        group: 'foo',
        title: 'Nuevo pedido #' + e.order.id,
        text: 'Hay un nuevo pedido en espera!',
        type: 'warn',
        duration: 20000
      });

      axios.get('/api/orders/' + e.order.id, {
        params: {
          plates: true
        }
      }).then(function (response) {
        _this3.orders.unshift(response.data);
      });
    });
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    var params = {
      restaurant_id: to.params.id,
      plates: true,
      start_date: moment__WEBPACK_IMPORTED_MODULE_0___default()(moment__WEBPACK_IMPORTED_MODULE_0___default()().subtract(1, 'days')).format("YYYY-MM-DD H:mm:ss")
    };
    axios.get('/api/orders', {
      params: params
    }).then(function (response) {
      next(function (vm) {
        return vm.orders = response.data;
      });
    });
  },
  computed: {
    filteredOrders: function filteredOrders() {
      var self = this;
      if (self.stateSelected == 2) return this.orders;
      return this.orders.filter(function (order) {
        return order.ready == self.stateSelected;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.my-style {\n  padding: 10px;\n  margin: 0 5px 5px;\n  font-size: 14px;\n  color: #ffffff;\n  background: #ffb648;\n  border-left: 5px solid #f48a06;\n}\n.card-ready {\n    border: 2px solid #28a745;\n    transition: 0.5s;\n}\n.card-waiting {\n    border: 2px solid #ffc107;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./MyOrders.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=template&id=db4338c6&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Order/MyOrders.vue?vue&type=template&id=db4338c6& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("notifications", {
        attrs: { group: "foo", position: "top center", classes: "my-style" },
      }),
      _vm._v(" "),
      _c("div", { staticClass: "card" }, [
        _c("h5", { staticClass: "card-header" }, [_vm._v("Pedidos")]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card-body" },
          [
            _c("div", { staticClass: "form-row" }, [
              _c("div", { staticClass: "form-group col-md-3" }, [
                _c("label", { attrs: { for: "inputState" } }, [
                  _vm._v("Periodo"),
                ]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.periodSelected,
                        expression: "periodSelected",
                      },
                    ],
                    staticClass: "form-control",
                    attrs: { id: "inputState" },
                    on: {
                      change: [
                        function ($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function (o) {
                              return o.selected
                            })
                            .map(function (o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.periodSelected = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        },
                        function ($event) {
                          return _vm.onChangePeriod($event)
                        },
                      ],
                    },
                  },
                  _vm._l(_vm.periods, function (period) {
                    return _c("option", { domProps: { value: period.value } }, [
                      _vm._v(
                        "\n                    " +
                          _vm._s(period.text) +
                          "\n                    "
                      ),
                    ])
                  }),
                  0
                ),
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group col-md-2" }, [
                _c("label", { attrs: { for: "inputState" } }, [
                  _vm._v("Estado"),
                ]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.stateSelected,
                        expression: "stateSelected",
                      },
                    ],
                    staticClass: "form-control",
                    attrs: { id: "inputState" },
                    on: {
                      change: [
                        function ($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function (o) {
                              return o.selected
                            })
                            .map(function (o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.stateSelected = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        },
                        function ($event) {
                          return _vm.onChangeState($event)
                        },
                      ],
                    },
                  },
                  _vm._l(_vm.states, function (state) {
                    return _c("option", { domProps: { value: state.value } }, [
                      _vm._v(
                        "\n                    " +
                          _vm._s(state.text) +
                          "\n                    "
                      ),
                    ])
                  }),
                  0
                ),
              ]),
            ]),
            _vm._v(" "),
            _vm._l(_vm.filteredOrders, function (order) {
              return _c(
                "div",
                {
                  key: order.id,
                  staticClass: "card",
                  class: {
                    "card-ready": order.ready,
                    "card-waiting": !order.ready,
                  },
                  staticStyle: { "margin-bottom": "10px" },
                },
                [
                  _c("h5", { staticClass: "card-header" }, [
                    _vm._v(
                      "Pedido #" + _vm._s(order.id) + "\n                "
                    ),
                    _c("span", { staticStyle: { color: "#a5a5a5" } }, [
                      _vm._v(" - " + _vm._s(_vm.onlyTime(order.created_at))),
                    ]),
                    _vm._v(" "),
                    !order.ready
                      ? _c(
                          "button",
                          {
                            staticClass: "btn btn-warning btn-sm",
                            staticStyle: { float: "right" },
                            attrs: { type: "button" },
                            on: {
                              click: function ($event) {
                                return _vm.setReady(order)
                              },
                            },
                          },
                          [
                            _c("font-awesome-icon", {
                              attrs: { icon: "bell" },
                            }),
                            _vm._v(" Listo\n                "),
                          ],
                          1
                        )
                      : _vm._e(),
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "card-body" },
                    [
                      order.sale
                        ? _c("p", [_vm._v(_vm._s(order.sale.name))])
                        : _vm._e(),
                      _c("p"),
                      _vm._l(order.plates, function (plate, index) {
                        return _c("p", { key: index }, [
                          _c("strong", [
                            _vm._v(
                              "\n                        " +
                                _vm._s(plate.pivot.quantity) +
                                "\n                        " +
                                _vm._s(plate.name) +
                                "(s)\n                        "
                            ),
                          ]),
                        ])
                      }),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(
                        "\n                    Comentarios: " +
                          _vm._s(order.comments) +
                          "\n                "
                      ),
                    ],
                    2
                  ),
                ]
              )
            }),
          ],
          2
        ),
      ]),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Order/MyOrders.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/Order/MyOrders.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MyOrders_vue_vue_type_template_id_db4338c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MyOrders.vue?vue&type=template&id=db4338c6& */ "./resources/js/components/Order/MyOrders.vue?vue&type=template&id=db4338c6&");
/* harmony import */ var _MyOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MyOrders.vue?vue&type=script&lang=js& */ "./resources/js/components/Order/MyOrders.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _MyOrders_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MyOrders.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _MyOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MyOrders_vue_vue_type_template_id_db4338c6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MyOrders_vue_vue_type_template_id_db4338c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Order/MyOrders.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Order/MyOrders.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/Order/MyOrders.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./MyOrders.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./MyOrders.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/Order/MyOrders.vue?vue&type=template&id=db4338c6&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Order/MyOrders.vue?vue&type=template&id=db4338c6& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_template_id_db4338c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./MyOrders.vue?vue&type=template&id=db4338c6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Order/MyOrders.vue?vue&type=template&id=db4338c6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_template_id_db4338c6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MyOrders_vue_vue_type_template_id_db4338c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=8.js.map