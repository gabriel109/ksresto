webpackJsonp([23],{

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(95)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(97)
/* template */
var __vue_template__ = __webpack_require__(98)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-85ef4954"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\Home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-85ef4954", Component.options)
  } else {
    hotAPI.reload("data-v-85ef4954", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(96);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(6)("734ef341", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js?sourceMap!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-85ef4954\",\"scoped\":true,\"hasInlineConfig\":true}!./new-age.css", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js?sourceMap!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-85ef4954\",\"scoped\":true,\"hasInlineConfig\":true}!./new-age.css");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 96:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(true);
// imports


// module
exports.push([module.i, "/*!\r\n * Start Bootstrap - New Age v5.0.0 (https://startbootstrap.com/template-overviews/new-age)\r\n * Copyright 2013-2018 Start Bootstrap\r\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-new-age/blob/master/LICENSE)\r\n */\nhtml[data-v-85ef4954],\r\nbody[data-v-85ef4954] {\r\n  width: 100%;\r\n  height: 100%;\r\n  margin-top:-25px;\n}\nbody[data-v-85ef4954] {\r\n  font-family: 'Muli', 'Helvetica', 'Arial', 'sans-serif';\n}\na[data-v-85ef4954] {\r\n  color: #fdcc52;\r\n  -webkit-transition: all .35s;\r\n  transition: all .35s;\n}\na[data-v-85ef4954]:hover, a[data-v-85ef4954]:focus {\r\n  color: #fcbd20;\n}\nhr[data-v-85ef4954] {\r\n  max-width: 100px;\r\n  margin: 25px auto 0;\r\n  border-width: 1px;\r\n  border-color: rgba(34, 34, 34, 0.1);\n}\nhr.light[data-v-85ef4954] {\r\n  border-color: white;\n}\nh1[data-v-85ef4954],\r\nh2[data-v-85ef4954],\r\nh3[data-v-85ef4954],\r\nh4[data-v-85ef4954],\r\nh5[data-v-85ef4954],\r\nh6[data-v-85ef4954] {\r\n  letter-spacing: 1px;\n}\np[data-v-85ef4954] {\r\n  font-size: 18px;\r\n  line-height: 1.5;\r\n  margin-bottom: 20px;\n}\nsection[data-v-85ef4954] {\r\n  padding: 100px 0;\n}\nsection h2[data-v-85ef4954] {\r\n  font-size: 30px;\n}\nheader.masthead[data-v-85ef4954] {\r\n  margin-top:-25px;\r\n  position: relative;\r\n  width: 100%;\r\n  padding-top: 150px;\r\n  padding-bottom: 100px;\r\n  /*color: white;*/\r\n  background: url(\"/images/food.png\");\r\n  /*background: url(\"/images/halftone-yellow.png\"), #477bae;\r\n  background: url(\"/images/halftone-yellow.png\"), -webkit-gradient(linear, right top, left top, from(#477bae), to(#55a9d1));\r\n  background: url(\"/images/halftone-yellow.png\"), linear-gradient(to left, #477bae, #55a9d1);*/\n}\nheader.masthead .header-content[data-v-85ef4954] {\r\n  max-width: 500px;\r\n  margin-bottom: 100px;\r\n  text-align: center;\n}\nheader.masthead .header-content h1[data-v-85ef4954] {\r\n  font-size: 30px;\n}\nheader.masthead .device-container[data-v-85ef4954] {\r\n  max-width: 325px;\r\n  margin-right: auto;\r\n  margin-left: auto;\n}\nheader.masthead .device-container .screen img[data-v-85ef4954] {\r\n  border-radius: 3px;\n}\n@media (min-width: 992px) {\nheader.masthead[data-v-85ef4954] {\r\n    height: 100vh;\r\n    min-height: 775px;\r\n    padding-top: 0;\r\n    padding-bottom: 0;\n}\nheader.masthead .header-content[data-v-85ef4954] {\r\n    margin-bottom: 0;\r\n    text-align: left;\n}\nheader.masthead .header-content h1[data-v-85ef4954] {\r\n    font-size: 50px;\n}\nsection h2[data-v-85ef4954] {\r\n    font-size: 50px;\n}\nheader.masthead .device-container[data-v-85ef4954] {\r\n    max-width: 325px;\n}\n}\nsection.download[data-v-85ef4954] {\r\n  position: relative;\r\n  padding: 150px 0;\n}\nsection.download h2[data-v-85ef4954] {\r\n  font-size: 50px;\r\n  margin-top: 0;\n}\nsection.download .badges .badge-link[data-v-85ef4954] {\r\n  display: block;\r\n  margin-bottom: 25px;\n}\nsection.download .badges .badge-link[data-v-85ef4954]:last-child {\r\n  margin-bottom: 0;\n}\nsection.download .badges .badge-link img[data-v-85ef4954] {\r\n  height: 60px;\n}\n@media (min-width: 768px) {\nsection.download .badges .badge-link[data-v-85ef4954] {\r\n    display: inline-block;\r\n    margin-bottom: 0;\n}\n}\n@media (min-width: 768px) {\nsection.download h2[data-v-85ef4954] {\r\n    font-size: 70px;\n}\n}\nsection.features[data-v-85ef4954] {\r\n  background: #f0f0f0;\r\n  background: -webkit-gradient(linear, left top, left bottom, from(#f0f0f0), to(#fff));\r\n  background: linear-gradient(#f0f0f0, #fff);\n}\nsection.features .section-heading[data-v-85ef4954] {\r\n  margin-bottom: 100px;\n}\nsection.features .section-heading h2[data-v-85ef4954] {\r\n  margin-top: 0;\n}\nsection.features .section-heading p[data-v-85ef4954] {\r\n  margin-bottom: 0;\n}\nsection.features .device-container[data-v-85ef4954],\r\nsection.features .feature-item[data-v-85ef4954] {\r\n  max-width: 325px;\r\n  margin: 0 auto;\n}\nsection.features .device-container[data-v-85ef4954] {\r\n  margin-bottom: 100px;\n}\n@media (min-width: 992px) {\nsection.features .device-container[data-v-85ef4954] {\r\n    margin-bottom: 0;\n}\n}\nsection.features .feature-item[data-v-85ef4954] {\r\n  padding-top: 50px;\r\n  padding-bottom: 50px;\r\n  text-align: center;\n}\nsection.features .feature-item h3[data-v-85ef4954] {\r\n  font-size: 30px;\n}\nsection.features .feature-item svg[data-v-85ef4954] {\r\n  font-size: 80px;\r\n  margin-bottom: 15px;\r\n  color: #f0b527;\r\n  -webkit-background-clip: text;\r\n  -webkit-text-fill-color: transparent;\n}\nsection.cta[data-v-85ef4954] {\r\n  position: relative;\r\n  padding: 250px 0;\r\n  background-image: url(\"/images/bg-cta.jpg\");\r\n  background-position: center;\r\n  background-size: cover;\n}\nsection.cta .cta-content[data-v-85ef4954] {\r\n  position: relative;\r\n  z-index: 1;\n}\nsection.cta .cta-content h2[data-v-85ef4954] {\r\n  font-size: 50px;\r\n  max-width: 450px;\r\n  margin-top: 0;\r\n  margin-bottom: 25px;\r\n  color: white;\n}\n@media (min-width: 768px) {\nsection.cta .cta-content h2[data-v-85ef4954] {\r\n    font-size: 60px;\n}\n}\nsection.cta .overlay[data-v-85ef4954] {\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  width: 100%;\r\n  height: 100%;\r\n  background-color: rgba(0, 0, 0, 0.5);\n}\nsection.contact[data-v-85ef4954] {\r\n  text-align: center;\n}\nsection.contact h2[data-v-85ef4954] {\r\n  margin-top: 0;\r\n  margin-bottom: 25px;\n}\nsection.contact h2 svg[data-v-85ef4954] {\r\n  color: #dd4b39;\n}\nsection.contact ul.list-social[data-v-85ef4954] {\r\n  margin-bottom: 0;\n}\nsection.contact ul.list-social li a[data-v-85ef4954] {\r\n  font-size: 40px;\r\n  line-height: 80px;\r\n  display: block;\r\n  width: 80px;\r\n  height: 80px;\r\n  color: white;\r\n  border-radius: 100%;\n}\nsection.contact ul.list-social li.social-twitter a[data-v-85ef4954] {\r\n  background-color: #1da1f2;\n}\nsection.contact ul.list-social li.social-twitter a[data-v-85ef4954]:hover {\r\n  background-color: #0d95e8;\n}\nsection.contact ul.list-social li.social-facebook a[data-v-85ef4954] {\r\n  background-color: #3b5998;\n}\nsection.contact ul.list-social li.social-facebook a[data-v-85ef4954]:hover {\r\n  background-color: #344e86;\n}\nsection.contact ul.list-social li.social-google-plus a[data-v-85ef4954] {\r\n  background-color: #dd4b39;\n}\nsection.contact ul.list-social li.social-google-plus a[data-v-85ef4954]:hover {\r\n  background-color: #d73925;\n}\nfooter[data-v-85ef4954] {\r\n  padding: 25px 0;\r\n  text-align: center;\r\n  color: rgba(255, 255, 255, 0.3);\r\n  background-color: #222222;\n}\nfooter p[data-v-85ef4954] {\r\n  font-size: 12px;\r\n  margin: 0;\n}\nfooter ul[data-v-85ef4954] {\r\n  margin-bottom: 0;\n}\nfooter ul li a[data-v-85ef4954] {\r\n  font-size: 12px;\r\n  color: rgba(255, 255, 255, 0.3);\n}\nfooter ul li a[data-v-85ef4954]:hover, footer ul li a[data-v-85ef4954]:focus, footer ul li a[data-v-85ef4954]:active, footer ul li a.active[data-v-85ef4954] {\r\n  text-decoration: none;\n}\n.bg-primary[data-v-85ef4954] {\r\n  background: #f0f0f0;\r\n  background: -webkit-gradient(linear, left top, left bottom, from(#f0f0f0), to(#fff));\r\n  background: linear-gradient(#f0f0f0, #fff);\r\n  /*background: -webkit-gradient(linear, left top, left bottom, from(#fdcc52), to(#fdc539));\r\n  background: linear-gradient(#fdcc52, #fdc539);*/\n}\n.text-primary[data-v-85ef4954] {\r\n  color: #fdcc52;\n}\n.no-gutter > [class*='col-'][data-v-85ef4954] {\r\n  padding-right: 0;\r\n  padding-left: 0;\n}\n.btn-outline-top[data-v-85ef4954] {\r\n  color: black;\r\n  border: 1px solid;\r\n  border-color: white;\n}\n.btn-outline-top[data-v-85ef4954]:hover, .btn-outline-top[data-v-85ef4954]:focus, .btn-outline-top[data-v-85ef4954]:active, .btn-outline-top.active[data-v-85ef4954] {\r\n  color: white;\r\n  border-color: #fdcc52;\r\n  background-color: #fdcc52;\n}\n.btn-outline[data-v-85ef4954] {\r\n  color: white;\r\n  border: 1px solid;\r\n  border-color: white;\n}\n.btn-outline[data-v-85ef4954]:hover, .btn-outline[data-v-85ef4954]:focus, .btn-outline[data-v-85ef4954]:active, .btn-outline.active[data-v-85ef4954] {\r\n  color: white;\r\n  border-color: #fdcc52;\r\n  background-color: #fdcc52;\n}\n.btn[data-v-85ef4954] {\r\n  border-radius: 0.5rem;\r\n  font-family: 'Lato', 'Helvetica', 'Arial', 'sans-serif';\r\n  letter-spacing: 2px;\r\n  text-transform: uppercase;\n}\n.btn-xl[data-v-85ef4954] {\r\n  font-size: 11px;\r\n  padding: 15px 45px;\n}\r\n", "", {"version":3,"sources":["C:/Apache24/htdocs/ksresto/public/css/new-age.css"],"names":[],"mappings":"AAAA;;;;GAIG;AAEH;;EAEE,YAAY;EACZ,aAAa;EACb,iBAAiB;CAClB;AAED;EACE,wDAAwD;CACzD;AAED;EACE,eAAe;EACf,6BAA6B;EAC7B,qBAAqB;CACtB;AAED;EACE,eAAe;CAChB;AAED;EACE,iBAAiB;EACjB,oBAAoB;EACpB,kBAAkB;EAClB,oCAAoC;CACrC;AAED;EACE,oBAAoB;CACrB;AAED;;;;;;EAME,oBAAoB;CACrB;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,oBAAoB;CACrB;AAED;EACE,iBAAiB;CAClB;AAED;EACE,gBAAgB;CACjB;AAID;EACE,iBAAiB;EACjB,mBAAmB;EACnB,YAAY;EACZ,mBAAmB;EACnB,sBAAsB;EACtB,iBAAiB;EACjB,oCAAoC;EACpC;;+FAE6F;CAC9F;AAED;EACE,iBAAiB;EACjB,qBAAqB;EACrB,mBAAmB;CACpB;AAED;EACE,gBAAgB;CACjB;AAED;EACE,iBAAiB;EACjB,mBAAmB;EACnB,kBAAkB;CACnB;AAED;EACE,mBAAmB;CACpB;AAED;AACE;IACE,cAAc;IACd,kBAAkB;IAClB,eAAe;IACf,kBAAkB;CACnB;AACD;IACE,iBAAiB;IACjB,iBAAiB;CAClB;AACD;IACE,gBAAgB;CACjB;AACD;IACE,gBAAgB;CACjB;AACD;IACE,iBAAiB;CAClB;CACF;AAED;EACE,mBAAmB;EACnB,iBAAiB;CAClB;AAED;EACE,gBAAgB;EAChB,cAAc;CACf;AAED;EACE,eAAe;EACf,oBAAoB;CACrB;AAED;EACE,iBAAiB;CAClB;AAED;EACE,aAAa;CACd;AAED;AACE;IACE,sBAAsB;IACtB,iBAAiB;CAClB;CACF;AAED;AACE;IACE,gBAAgB;CACjB;CACF;AAED;EACE,oBAAoB;EAEpB,qFAA2C;EAA3C,2CAA2C;CAC5C;AAED;EACE,qBAAqB;CACtB;AAED;EACE,cAAc;CACf;AAED;EACE,iBAAiB;CAClB;AAED;;EAEE,iBAAiB;EACjB,eAAe;CAChB;AAED;EACE,qBAAqB;CACtB;AAED;AACE;IACE,iBAAiB;CAClB;CACF;AAED;EACE,kBAAkB;EAClB,qBAAqB;EACrB,mBAAmB;CACpB;AAED;EACE,gBAAgB;CACjB;AAED;EACE,gBAAgB;EAChB,oBAAoB;EACpB,eAAe;EACf,8BAA8B;EAC9B,qCAAqC;CACtC;AAED;EACE,mBAAmB;EACnB,iBAAiB;EACjB,4CAA4C;EAC5C,4BAA4B;EAC5B,uBAAuB;CACxB;AAED;EACE,mBAAmB;EACnB,WAAW;CACZ;AAED;EACE,gBAAgB;EAChB,iBAAiB;EACjB,cAAc;EACd,oBAAoB;EACpB,aAAa;CACd;AAED;AACE;IACE,gBAAgB;CACjB;CACF;AAED;EACE,mBAAmB;EACnB,OAAO;EACP,QAAQ;EACR,YAAY;EACZ,aAAa;EACb,qCAAqC;CACtC;AAED;EACE,mBAAmB;CACpB;AAED;EACE,cAAc;EACd,oBAAoB;CACrB;AAED;EACE,eAAe;CAChB;AAED;EACE,iBAAiB;CAClB;AAED;EACE,gBAAgB;EAChB,kBAAkB;EAClB,eAAe;EACf,YAAY;EACZ,aAAa;EACb,aAAa;EACb,oBAAoB;CACrB;AAED;EACE,0BAA0B;CAC3B;AAED;EACE,0BAA0B;CAC3B;AAED;EACE,0BAA0B;CAC3B;AAED;EACE,0BAA0B;CAC3B;AAED;EACE,0BAA0B;CAC3B;AAED;EACE,0BAA0B;CAC3B;AAED;EACE,gBAAgB;EAChB,mBAAmB;EACnB,gCAAgC;EAChC,0BAA0B;CAC3B;AAED;EACE,gBAAgB;EAChB,UAAU;CACX;AAED;EACE,iBAAiB;CAClB;AAED;EACE,gBAAgB;EAChB,gCAAgC;CACjC;AAED;EACE,sBAAsB;CACvB;AAED;EACE,oBAAoB;EACpB,qFAA2C;EAA3C,2CAA2C;EAC3C;kDACgD;CACjD;AAED;EACE,eAAe;CAChB;AAED;EACE,iBAAiB;EACjB,gBAAgB;CACjB;AAED;EACE,aAAa;EACb,kBAAkB;EAClB,oBAAoB;CACrB;AACD;EACE,aAAa;EACb,sBAAsB;EACtB,0BAA0B;CAC3B;AACD;EACE,aAAa;EACb,kBAAkB;EAClB,oBAAoB;CACrB;AAED;EACE,aAAa;EACb,sBAAsB;EACtB,0BAA0B;CAC3B;AAED;EACE,sBAAsB;EACtB,wDAAwD;EACxD,oBAAoB;EACpB,0BAA0B;CAC3B;AAED;EACE,gBAAgB;EAChB,mBAAmB;CACpB","file":"new-age.css","sourcesContent":["/*!\r\n * Start Bootstrap - New Age v5.0.0 (https://startbootstrap.com/template-overviews/new-age)\r\n * Copyright 2013-2018 Start Bootstrap\r\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-new-age/blob/master/LICENSE)\r\n */\r\n\r\nhtml,\r\nbody {\r\n  width: 100%;\r\n  height: 100%;\r\n  margin-top:-25px;\r\n}\r\n\r\nbody {\r\n  font-family: 'Muli', 'Helvetica', 'Arial', 'sans-serif';\r\n}\r\n\r\na {\r\n  color: #fdcc52;\r\n  -webkit-transition: all .35s;\r\n  transition: all .35s;\r\n}\r\n\r\na:hover, a:focus {\r\n  color: #fcbd20;\r\n}\r\n\r\nhr {\r\n  max-width: 100px;\r\n  margin: 25px auto 0;\r\n  border-width: 1px;\r\n  border-color: rgba(34, 34, 34, 0.1);\r\n}\r\n\r\nhr.light {\r\n  border-color: white;\r\n}\r\n\r\nh1,\r\nh2,\r\nh3,\r\nh4,\r\nh5,\r\nh6 {\r\n  letter-spacing: 1px;\r\n}\r\n\r\np {\r\n  font-size: 18px;\r\n  line-height: 1.5;\r\n  margin-bottom: 20px;\r\n}\r\n\r\nsection {\r\n  padding: 100px 0;\r\n}\r\n\r\nsection h2 {\r\n  font-size: 30px;\r\n}\r\n\r\n\r\n\r\nheader.masthead {\r\n  margin-top:-25px;\r\n  position: relative;\r\n  width: 100%;\r\n  padding-top: 150px;\r\n  padding-bottom: 100px;\r\n  /*color: white;*/\r\n  background: url(\"/images/food.png\");\r\n  /*background: url(\"/images/halftone-yellow.png\"), #477bae;\r\n  background: url(\"/images/halftone-yellow.png\"), -webkit-gradient(linear, right top, left top, from(#477bae), to(#55a9d1));\r\n  background: url(\"/images/halftone-yellow.png\"), linear-gradient(to left, #477bae, #55a9d1);*/\r\n}\r\n\r\nheader.masthead .header-content {\r\n  max-width: 500px;\r\n  margin-bottom: 100px;\r\n  text-align: center;\r\n}\r\n\r\nheader.masthead .header-content h1 {\r\n  font-size: 30px;\r\n}\r\n\r\nheader.masthead .device-container {\r\n  max-width: 325px;\r\n  margin-right: auto;\r\n  margin-left: auto;\r\n}\r\n\r\nheader.masthead .device-container .screen img {\r\n  border-radius: 3px;\r\n}\r\n\r\n@media (min-width: 992px) {\r\n  header.masthead {\r\n    height: 100vh;\r\n    min-height: 775px;\r\n    padding-top: 0;\r\n    padding-bottom: 0;\r\n  }\r\n  header.masthead .header-content {\r\n    margin-bottom: 0;\r\n    text-align: left;\r\n  }\r\n  header.masthead .header-content h1 {\r\n    font-size: 50px;\r\n  }\r\n  section h2 {\r\n    font-size: 50px;\r\n  }\r\n  header.masthead .device-container {\r\n    max-width: 325px;\r\n  }\r\n}\r\n\r\nsection.download {\r\n  position: relative;\r\n  padding: 150px 0;\r\n}\r\n\r\nsection.download h2 {\r\n  font-size: 50px;\r\n  margin-top: 0;\r\n}\r\n\r\nsection.download .badges .badge-link {\r\n  display: block;\r\n  margin-bottom: 25px;\r\n}\r\n\r\nsection.download .badges .badge-link:last-child {\r\n  margin-bottom: 0;\r\n}\r\n\r\nsection.download .badges .badge-link img {\r\n  height: 60px;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  section.download .badges .badge-link {\r\n    display: inline-block;\r\n    margin-bottom: 0;\r\n  }\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  section.download h2 {\r\n    font-size: 70px;\r\n  }\r\n}\r\n\r\nsection.features {\r\n  background: #f0f0f0;\r\n  background: -webkit-gradient(linear, left top, left bottom, from(#f0f0f0), to(#fff));\r\n  background: linear-gradient(#f0f0f0, #fff);\r\n}\r\n\r\nsection.features .section-heading {\r\n  margin-bottom: 100px;\r\n}\r\n\r\nsection.features .section-heading h2 {\r\n  margin-top: 0;\r\n}\r\n\r\nsection.features .section-heading p {\r\n  margin-bottom: 0;\r\n}\r\n\r\nsection.features .device-container,\r\nsection.features .feature-item {\r\n  max-width: 325px;\r\n  margin: 0 auto;\r\n}\r\n\r\nsection.features .device-container {\r\n  margin-bottom: 100px;\r\n}\r\n\r\n@media (min-width: 992px) {\r\n  section.features .device-container {\r\n    margin-bottom: 0;\r\n  }\r\n}\r\n\r\nsection.features .feature-item {\r\n  padding-top: 50px;\r\n  padding-bottom: 50px;\r\n  text-align: center;\r\n}\r\n\r\nsection.features .feature-item h3 {\r\n  font-size: 30px;\r\n}\r\n\r\nsection.features .feature-item svg {\r\n  font-size: 80px;\r\n  margin-bottom: 15px;\r\n  color: #f0b527;\r\n  -webkit-background-clip: text;\r\n  -webkit-text-fill-color: transparent;\r\n}\r\n\r\nsection.cta {\r\n  position: relative;\r\n  padding: 250px 0;\r\n  background-image: url(\"/images/bg-cta.jpg\");\r\n  background-position: center;\r\n  background-size: cover;\r\n}\r\n\r\nsection.cta .cta-content {\r\n  position: relative;\r\n  z-index: 1;\r\n}\r\n\r\nsection.cta .cta-content h2 {\r\n  font-size: 50px;\r\n  max-width: 450px;\r\n  margin-top: 0;\r\n  margin-bottom: 25px;\r\n  color: white;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  section.cta .cta-content h2 {\r\n    font-size: 60px;\r\n  }\r\n}\r\n\r\nsection.cta .overlay {\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  width: 100%;\r\n  height: 100%;\r\n  background-color: rgba(0, 0, 0, 0.5);\r\n}\r\n\r\nsection.contact {\r\n  text-align: center;\r\n}\r\n\r\nsection.contact h2 {\r\n  margin-top: 0;\r\n  margin-bottom: 25px;\r\n}\r\n\r\nsection.contact h2 svg {\r\n  color: #dd4b39;\r\n}\r\n\r\nsection.contact ul.list-social {\r\n  margin-bottom: 0;\r\n}\r\n\r\nsection.contact ul.list-social li a {\r\n  font-size: 40px;\r\n  line-height: 80px;\r\n  display: block;\r\n  width: 80px;\r\n  height: 80px;\r\n  color: white;\r\n  border-radius: 100%;\r\n}\r\n\r\nsection.contact ul.list-social li.social-twitter a {\r\n  background-color: #1da1f2;\r\n}\r\n\r\nsection.contact ul.list-social li.social-twitter a:hover {\r\n  background-color: #0d95e8;\r\n}\r\n\r\nsection.contact ul.list-social li.social-facebook a {\r\n  background-color: #3b5998;\r\n}\r\n\r\nsection.contact ul.list-social li.social-facebook a:hover {\r\n  background-color: #344e86;\r\n}\r\n\r\nsection.contact ul.list-social li.social-google-plus a {\r\n  background-color: #dd4b39;\r\n}\r\n\r\nsection.contact ul.list-social li.social-google-plus a:hover {\r\n  background-color: #d73925;\r\n}\r\n\r\nfooter {\r\n  padding: 25px 0;\r\n  text-align: center;\r\n  color: rgba(255, 255, 255, 0.3);\r\n  background-color: #222222;\r\n}\r\n\r\nfooter p {\r\n  font-size: 12px;\r\n  margin: 0;\r\n}\r\n\r\nfooter ul {\r\n  margin-bottom: 0;\r\n}\r\n\r\nfooter ul li a {\r\n  font-size: 12px;\r\n  color: rgba(255, 255, 255, 0.3);\r\n}\r\n\r\nfooter ul li a:hover, footer ul li a:focus, footer ul li a:active, footer ul li a.active {\r\n  text-decoration: none;\r\n}\r\n\r\n.bg-primary {\r\n  background: #f0f0f0;\r\n  background: linear-gradient(#f0f0f0, #fff);\r\n  /*background: -webkit-gradient(linear, left top, left bottom, from(#fdcc52), to(#fdc539));\r\n  background: linear-gradient(#fdcc52, #fdc539);*/\r\n}\r\n\r\n.text-primary {\r\n  color: #fdcc52;\r\n}\r\n\r\n.no-gutter > [class*='col-'] {\r\n  padding-right: 0;\r\n  padding-left: 0;\r\n}\r\n\r\n.btn-outline-top {\r\n  color: black;\r\n  border: 1px solid;\r\n  border-color: white;\r\n}\r\n.btn-outline-top:hover, .btn-outline-top:focus, .btn-outline-top:active, .btn-outline-top.active {\r\n  color: white;\r\n  border-color: #fdcc52;\r\n  background-color: #fdcc52;\r\n}\r\n.btn-outline {\r\n  color: white;\r\n  border: 1px solid;\r\n  border-color: white;\r\n}\r\n\r\n.btn-outline:hover, .btn-outline:focus, .btn-outline:active, .btn-outline.active {\r\n  color: white;\r\n  border-color: #fdcc52;\r\n  background-color: #fdcc52;\r\n}\r\n\r\n.btn {\r\n  border-radius: 0.5rem;\r\n  font-family: 'Lato', 'Helvetica', 'Arial', 'sans-serif';\r\n  letter-spacing: 2px;\r\n  text-transform: uppercase;\r\n}\r\n\r\n.btn-xl {\r\n  font-size: 11px;\r\n  padding: 15px 45px;\r\n}\r\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  mounted: function mounted() {},

  methods: {}
});

/***/ }),

/***/ 98:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("header", { staticClass: "masthead" }, [
      _c("div", { staticClass: "container h-100" }, [
        _c("div", { staticClass: "row h-100" }, [
          _c("div", { staticClass: "col-lg-7 my-auto" }, [
            _c(
              "div",
              { staticClass: "header-content mx-auto" },
              [
                _c("h1", { staticClass: "mb-3" }, [
                  _vm._v("Con organización todo funciona mejor.")
                ]),
                _vm._v(" "),
                _c("h3", { staticClass: "mb-5" }, [
                  _vm._v("Pensado para su restaurant, bar o delivery.")
                ]),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-outline-top btn-xl js-scroll-trigger",
                    attrs: { to: { name: "register" }, tag: "a" }
                  },
                  [_vm._v("Empezar gratis ya!")]
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _vm._m(0)
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "features", attrs: { id: "features" } }, [
      _c("div", { staticClass: "container" }, [
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _vm._m(2),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-8 my-auto" }, [
            _c("div", { staticClass: "container-fluid" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-lg-6" }, [
                  _c(
                    "div",
                    { staticClass: "feature-item" },
                    [
                      _c("font-awesome-icon", {
                        staticClass: "fa-10px",
                        attrs: { icon: "book-open" }
                      }),
                      _vm._v(" "),
                      _c("h3", [_vm._v("Pedidos")]),
                      _vm._v(" "),
                      _c("p", { staticClass: "text-muted" }, [
                        _vm._v(
                          "Los mozos registran los pedidos desde su celular!"
                        )
                      ])
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-6" }, [
                  _c(
                    "div",
                    { staticClass: "feature-item" },
                    [
                      _c("font-awesome-icon", {
                        staticClass: "fa-10px",
                        attrs: { icon: "mobile-alt" }
                      }),
                      _vm._v(" "),
                      _c("h3", [_vm._v("Adaptable")]),
                      _vm._v(" "),
                      _c("p", { staticClass: "text-muted" }, [
                        _vm._v("Listo para usar desde cualquier dispositivo!")
                      ])
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-lg-6" }, [
                  _c(
                    "div",
                    { staticClass: "feature-item" },
                    [
                      _c("font-awesome-icon", {
                        staticClass: "fa-10px",
                        attrs: { icon: "concierge-bell" }
                      }),
                      _vm._v(" "),
                      _c("h3", [_vm._v("Cocina")]),
                      _vm._v(" "),
                      _c("p", { staticClass: "text-muted" }, [
                        _vm._v(
                          "La cocina recibe notificaciones de los nuevos pedidos!"
                        )
                      ])
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-6" }, [
                  _c(
                    "div",
                    { staticClass: "feature-item" },
                    [
                      _c("font-awesome-icon", {
                        staticClass: "fa-10px",
                        attrs: { icon: "chart-line" }
                      }),
                      _vm._v(" "),
                      _c("h3", [_vm._v("Reportes")]),
                      _vm._v(" "),
                      _c("p", { staticClass: "text-muted" }, [
                        _vm._v("Vea como crece su negocio!")
                      ])
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "cta" }, [
      _c("div", { staticClass: "cta-content" }, [
        _c(
          "div",
          { staticClass: "container" },
          [
            _vm._m(3),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "btn btn-outline btn-xl js-scroll-trigger",
                attrs: { to: { name: "services" }, tag: "a" }
              },
              [_vm._v("Ver Precios")]
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "overlay" })
    ]),
    _vm._v(" "),
    _vm._m(4),
    _vm._v(" "),
    _vm._m(5)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-5 my-auto" }, [
      _c("div", { staticClass: "device-container" }, [
        _c(
          "div",
          { staticClass: "device-mockup iphone6_plus portrait white" },
          [
            _c("div", { staticClass: "device" }, [
              _c("div", { staticClass: "screen" }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "/images/demo-screen-1.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "button" })
            ])
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "section-heading text-center" }, [
      _c("h2", [_vm._v("Vos en lo tuyo, nosotros en lo nuestro.")]),
      _vm._v(" "),
      _c("p", { staticClass: "text-muted" }, [
        _vm._v("Pruebalo gratis por el tiempo que quieras!")
      ]),
      _vm._v(" "),
      _c("hr")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-4 my-auto" }, [
      _c("div", { staticClass: "device-container" }, [
        _c(
          "div",
          { staticClass: "device-mockup iphone6_plus portrait white" },
          [
            _c("div", { staticClass: "device" }, [
              _c("div", { staticClass: "screen" }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "/images/demo-screen-1.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "button" })
            ])
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h2", [
      _vm._v("Deja la espera."),
      _c("br"),
      _vm._v("Empieza la mejora.")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      { staticClass: "contact bg-primary", attrs: { id: "contact" } },
      [
        _c("div", { staticClass: "container" }, [
          _c("ul", { staticClass: "list-inline list-social" }, [
            _c("li", { staticClass: "list-inline-item social-twitter" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("i", { staticClass: "fab fa-twitter" })
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "list-inline-item social-facebook" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("i", { staticClass: "fab fa-facebook-f" })
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "list-inline-item social-google-plus" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("i", { staticClass: "fab fa-google-plus-g" })
              ])
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", [
      _c("div", { staticClass: "container" }, [
        _c("p", [_vm._v("© KSResto 2018. All Rights Reserved.")]),
        _vm._v(" "),
        _c("ul", { staticClass: "list-inline" }, [
          _c("li", { staticClass: "list-inline-item" }, [
            _c("a", { attrs: { href: "#" } }, [_vm._v("Privacidad")])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "list-inline-item" }, [
            _c("a", { attrs: { href: "#" } }, [_vm._v("Terminos")])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "list-inline-item" }, [
            _c("a", { attrs: { href: "#" } }, [_vm._v("FAQ")])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-85ef4954", module.exports)
  }
}

/***/ })

});
//# sourceMappingURL=23.js.map