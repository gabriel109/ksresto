(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Restaurant/ReportSales.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Restaurant/ReportSales.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var v_charts_lib_line_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! v-charts/lib/line.common */ "./node_modules/v-charts/lib/line.common.js");
/* harmony import */ var v_charts_lib_line_common__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(v_charts_lib_line_common__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue2_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue2-datepicker */ "./node_modules/vue2-datepicker/lib/index.js");
/* harmony import */ var vue2_datepicker__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue2_datepicker__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id'],
  components: {
    VeLine: v_charts_lib_line_common__WEBPACK_IMPORTED_MODULE_1___default.a,
    DatePicker: vue2_datepicker__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  data: function data() {
    var _this = this;

    return {
      laravelData: {},
      chartData: {
        columns: ['date', 'income'],
        rows: []
      },
      chartSettings: {
        labelMap: {
          income: 'Ingresos'
        },
        dimension: ['date']
      },
      date: null,
      shortcuts: [{
        text: 'Hoy',
        onClick: function onClick() {
          _this.date = [new Date(), new Date()];
        }
      }, {
        text: 'Ayer',
        onClick: function onClick() {
          var yesterday = new Date();
          yesterday.setDate(yesterday.getDate() - 1);
          _this.date = [yesterday, yesterday];
        }
      }, {
        text: 'Ultimos 30 días',
        onClick: function onClick() {
          var start = new Date();
          start.setDate(start.getDate() - 30);
          _this.date = [start, new Date()];
        }
      }]
    };
  },
  mounted: function mounted() {
    moment__WEBPACK_IMPORTED_MODULE_0___default.a.locale('es');
  },
  methods: {
    getGraphic: function getGraphic() {
      var _this2 = this;

      var params = {
        start_date: moment__WEBPACK_IMPORTED_MODULE_0___default()(this.date[0]).format('YYYY-MM-DD'),
        end_date: moment__WEBPACK_IMPORTED_MODULE_0___default()(this.date[1]).format('YYYY-MM-DD')
      };
      axios.get('/api/restaurants/' + this.id + '/reportSales', {
        params: params
      }).then(function (response) {
        _this2.chartData.rows = Object.assign([], response.data).reverse(); // Cast from int to string

        Object.keys(_this2.chartData.rows).forEach(function (key) {
          _this2.chartData.rows[key].date = moment__WEBPACK_IMPORTED_MODULE_0___default()(_this2.chartData.rows[key].date, 'YYYY-MM-DD').format('DD-MM-YYYY');
        });
      });
    },
    onDateSelected: function onDateSelected(date) {
      this.getGraphic();
    }
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      var start = new Date();
      start.setDate(start.getDate() - 30);
      vm.date = new Array(start, new Date());
      vm.getGraphic();
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Restaurant/ReportSales.vue?vue&type=template&id=47141571&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Restaurant/ReportSales.vue?vue&type=template&id=47141571& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "card" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "card-body" },
        [
          _c("date-picker", {
            attrs: {
              range: "",
              lang: "es",
              format: "DD-MM-YYYY",
              clearable: false,
              shortcuts: _vm.shortcuts,
              "not-after": new Date(),
              confirm: "",
            },
            on: { confirm: _vm.onDateSelected },
            model: {
              value: _vm.date,
              callback: function ($$v) {
                _vm.date = $$v
              },
              expression: "date",
            },
          }),
          _vm._v(" "),
          _c("ve-line", {
            attrs: { data: _vm.chartData, settings: _vm.chartSettings },
          }),
        ],
        1
      ),
    ]),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h5", { staticStyle: { float: "left", "padding-top": "5px" } }, [
        _vm._v("Ventas"),
      ]),
    ])
  },
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Restaurant/ReportSales.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Restaurant/ReportSales.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ReportSales_vue_vue_type_template_id_47141571___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ReportSales.vue?vue&type=template&id=47141571& */ "./resources/js/components/Restaurant/ReportSales.vue?vue&type=template&id=47141571&");
/* harmony import */ var _ReportSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ReportSales.vue?vue&type=script&lang=js& */ "./resources/js/components/Restaurant/ReportSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ReportSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ReportSales_vue_vue_type_template_id_47141571___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ReportSales_vue_vue_type_template_id_47141571___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Restaurant/ReportSales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Restaurant/ReportSales.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Restaurant/ReportSales.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ReportSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ReportSales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Restaurant/ReportSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ReportSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Restaurant/ReportSales.vue?vue&type=template&id=47141571&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Restaurant/ReportSales.vue?vue&type=template&id=47141571& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReportSales_vue_vue_type_template_id_47141571___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ReportSales.vue?vue&type=template&id=47141571& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Restaurant/ReportSales.vue?vue&type=template&id=47141571&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReportSales_vue_vue_type_template_id_47141571___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReportSales_vue_vue_type_template_id_47141571___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=13.js.map