webpackJsonp([22],{

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(156);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(6)("5bfacc15", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c04cbf54\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MyOrders.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c04cbf54\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MyOrders.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(true);
// imports


// module
exports.push([module.i, "\n.my-style{\r\n  padding: 10px;\r\n  margin: 0 5px 5px;\r\n\r\n  font-size: 12px;\r\n\r\n  color: #ffffff;\r\n  background: #44A4FC;\r\n  border-left: 5px solid #187FE7;\n&.warn {\r\n    background: #ffb648;\r\n    border-left-color: #f48a06;\n}\n&.error {\r\n    background: #E54D42;\r\n    border-left-color: #B82E24;\n}\n&.success {\r\n    background: #68CD86;\r\n    border-left-color: #42A85F;\n}\n}\r\n", "", {"version":3,"sources":["C:/Apache24/htdocs/ksresto/resources/assets/js/components/Order/resources/assets/js/components/Order/MyOrders.vue"],"names":[],"mappings":";AA+DA;EACA,cAAA;EACA,kBAAA;;EAEA,gBAAA;;EAEA,eAAA;EACA,oBAAA;EACA,+BAAA;AAEA;IACA,oBAAA;IACA,2BAAA;CACA;AAEA;IACA,oBAAA;IACA,2BAAA;CACA;AAEA;IACA,oBAAA;IACA,2BAAA;CACA;CACA","file":"MyOrders.vue","sourcesContent":["<template>\r\n    <div>\r\n        <notifications group=\"foo\" position=\"top center\" />\r\n        <div class=\"card\">\r\n            <h5 class=\"card-header\">Pedidos</h5>\r\n            <div class=\"card-body\">\r\n                <div class=\"card\" v-for=\"order in orders\" :key=\"order.id\" style=\"margin-bottom:10px\">\r\n                    <h5 class=\"card-header\">Pedido #{{ order.id }} - {{ onlyTime(order.created_at) }}</h5>\r\n                    <div class=\"card-body\">\r\n                        <p v-if=\"order.sale\">{{ order.sale.name }}<p>\r\n                        <p v-for=\"(plate,index) in order.plates\" :key=\"index\">\r\n                            <strong>\r\n                            {{ plate.pivot.quantity }}\r\n                            {{ plate.name }}(s)\r\n                            </strong>\r\n                        </p>\r\n                        <hr />\r\n                        Comentarios: {{ order.comments }}\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</template>\r\n\r\n<script>\r\n\r\n    export default {\r\n        props: ['id'], //restaurant_id\r\n        data(){\r\n            return {\r\n                orders: [],\r\n            }\r\n        },\r\n        methods: {\r\n        },\r\n        mounted() {\r\n            Echo.private('orders.'+this.id)\r\n                .listen('OrderStoredEvent', (e) => {\r\n                    this.$notify({\r\n                        group: 'foo',\r\n                        title: 'Nuevo pedido #' + e.order.id,\r\n                        text: 'Hay un nuevo pedido en espera!',\r\n                        type: 'warn'\r\n                    });\r\n                    axios.get('/api/orders/' + e.order.id, {params: {plates: true}}).then((response) => {\r\n                        this.orders.unshift(response.data);\r\n                    })\r\n                })\r\n        },\r\n        beforeRouteEnter (to, from, next) {\r\n            const params = {\r\n                restaurant_id: to.params.id,\r\n                plates: true\r\n            };\r\n            axios.get('/api/orders', {params}).then((response) => {\r\n                next(vm => (vm.orders = response.data) )\r\n            })\r\n        }\r\n    }\r\n</script>\r\n\r\n<style>\r\n.my-style{\r\n  padding: 10px;\r\n  margin: 0 5px 5px;\r\n\r\n  font-size: 12px;\r\n\r\n  color: #ffffff;\r\n  background: #44A4FC;\r\n  border-left: 5px solid #187FE7;\r\n\r\n  &.warn {\r\n    background: #ffb648;\r\n    border-left-color: #f48a06;\r\n  }\r\n\r\n  &.error {\r\n    background: #E54D42;\r\n    border-left-color: #B82E24;\r\n  }\r\n\r\n  &.success {\r\n    background: #68CD86;\r\n    border-left-color: #42A85F;\r\n  }\r\n}\r\n</style>"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['id'], //restaurant_id
    data: function data() {
        return {
            orders: []
        };
    },

    methods: {},
    mounted: function mounted() {
        var _this = this;

        Echo.private('orders.' + this.id).listen('OrderStoredEvent', function (e) {
            _this.$notify({
                group: 'foo',
                title: 'Nuevo pedido #' + e.order.id,
                text: 'Hay un nuevo pedido en espera!',
                type: 'warn'
            });
            axios.get('/api/orders/' + e.order.id, { params: { plates: true } }).then(function (response) {
                _this.orders.unshift(response.data);
            });
        });
    },
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        var params = {
            restaurant_id: to.params.id,
            plates: true
        };
        axios.get('/api/orders', { params: params }).then(function (response) {
            next(function (vm) {
                return vm.orders = response.data;
            });
        });
    }
});

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("notifications", { attrs: { group: "foo", position: "top center" } }),
      _vm._v(" "),
      _c("div", { staticClass: "card" }, [
        _c("h5", { staticClass: "card-header" }, [_vm._v("Pedidos")]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card-body" },
          _vm._l(_vm.orders, function(order) {
            return _c(
              "div",
              {
                key: order.id,
                staticClass: "card",
                staticStyle: { "margin-bottom": "10px" }
              },
              [
                _c("h5", { staticClass: "card-header" }, [
                  _vm._v(
                    "Pedido #" +
                      _vm._s(order.id) +
                      " - " +
                      _vm._s(_vm.onlyTime(order.created_at))
                  )
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    order.sale
                      ? _c("p", [_vm._v(_vm._s(order.sale.name))])
                      : _vm._e(),
                    _c("p"),
                    _vm._l(order.plates, function(plate, index) {
                      return _c("p", { key: index }, [
                        _c("strong", [
                          _vm._v(
                            "\n                        " +
                              _vm._s(plate.pivot.quantity) +
                              "\n                        " +
                              _vm._s(plate.name) +
                              "(s)\n                        "
                          )
                        ])
                      ])
                    }),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(
                      "\n                    Comentarios: " +
                        _vm._s(order.comments) +
                        "\n                "
                    )
                  ],
                  2
                )
              ]
            )
          })
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c04cbf54", module.exports)
  }
}

/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(155)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(157)
/* template */
var __vue_template__ = __webpack_require__(158)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\Order\\MyOrders.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c04cbf54", Component.options)
  } else {
    hotAPI.reload("data-v-c04cbf54", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});
//# sourceMappingURL=22.js.map